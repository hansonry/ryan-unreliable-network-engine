#include <string.h>
#include <runeWritable.h>


static inline
size_t runeaWrite(struct rune_writable * writable, 
                  size_t count, size_t elementSize, 
                  const void * data)
{
   struct runew_array * array = (struct runew_array *)writable;
   size_t bytesToWrite = count * elementSize;
   memcpy(array->array, data, bytesToWrite);
   array->array += bytesToWrite;
   return bytesToWrite;
}

const struct rune_writable_vt WritableArrayVT =
{
   runeaWrite
};
