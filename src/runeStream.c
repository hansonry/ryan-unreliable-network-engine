#include <stdlib.h>
#include <runeStream.h>
#include <runeRefCount.h>
#include <runeList.h>
#include <runeByteQueue.h>

// # Connection States
// 
// Start at: Disconnected
// Disconnected -> RemoteConnected: if remoteDesires connection
// Disconnected -> Local
// 
// # Example
//
// Expected to retransmit dropped messages. 
//
// Local <-> Remmote
//
// -> Like to Connect 0
// <- NACK 0
// -> Like to Connect 0
// <- ACK 0
// ... At this point we are connected and data can flow
// <- Like to Disconnect 0
//
//

// 

enum rune_stream_state
{
   eRUNESS_Closed, // We are closed, send NACKs
   eRUNESS_ConnectingRequested, // We just sent our connection request message and we are waiting
   eRUNESS_ConnectingWait, // Our connection request message got NACKed
   eRUNESS_Connected, // Our connection request was ACKed or we got a connection request
};



RUNE_LIST_TYPE_CREATE(stream, struct rune_stream *)

struct rune_stream
{
   struct rune_module_stream * owner;
   struct rune_refcount refCount;
   struct rune_bytequeue queueRx;
   struct rune_bytequeue queueTx;
   streamId_t id;
   enum rune_stream_state state;
   bool keepConnected;
};

struct rune_module_stream
{
   struct rune_refcount refCount;
   struct rune_connection * connection;
   struct rune_list_stream streamList;
};

struct rune_module_stream * runemsNew(struct rune_connection * connection)
{
   struct rune_module_stream * mStream = malloc(sizeof(struct rune_module_stream));
   runercInit(&mStream->refCount);
   runelInit_stream(&mStream->streamList, 0, 0);
   mStream->connection = connection;

   runecTake(mStream->connection);
   return mStream;
}


void runemsTake(struct rune_module_stream * mStream)
{
   runercTake(&mStream->refCount);
}

void runemsRelease(struct rune_module_stream * mStream)
{
   if(runercRelease(&mStream->refCount))
   {
      runecRelease(mStream->connection);
      runelCleanup_stream(&mStream->streamList);
      free(mStream);
   }
}

struct rune_stream * runemsFindStream(struct rune_module_stream * mStream, streamId_t id, size_t * foundIndex)
{
   size_t i, count;
   struct rune_stream ** streams = runelGetAll_stream(&mStream->streamList, &count);

   for(i = 0; i < count; i ++)
   {
      if(streams[i]->id == id)
      {
         if(foundIndex != NULL)
         {
            (*foundIndex) = i;
         }
         return streams[i];
      }
   }
   return NULL;
}

static inline
void runesStateUpdate(struct rune_stream * stream)
{
   if(stream->keepConnected)
   {
   }
   else
   {
   }
}

static inline
struct rune_stream * runesNew(struct rune_module_stream * owner, streamId_t id)
{
   struct rune_stream * stream = malloc(sizeof(struct rune_stream));
   stream->owner = owner;
   stream->id = id;
   runercInit(&stream->refCount); 
   runebqInit(&stream->queueRx, 0, 0);
   runebqInit(&stream->queueTx, 0, 0);
   stream->keepConnected = true;
   stream->state = eRUNESS_Closed;
   runesStateUpdate(stream);
   return stream;
}

struct rune_stream * runemsGetStream(struct rune_module_stream * mStream, streamId_t id)
{
   struct rune_stream * stream = runemsFindStream(mStream, id, NULL);

   if(stream == NULL)
   {
      stream = runesNew(mStream, id);
   }
   return stream;
}

void runesTake(struct rune_stream * stream)
{
   runercTake(&stream->refCount);
}

void runesRelease(struct rune_stream * stream)
{
   if(runercRelease(&stream->refCount))
   {
      runebqCleanup(&stream->queueRx);
      runebqCleanup(&stream->queueTx);
      free(stream);
   }
}

size_t runesWrite(struct rune_stream * stream, const void * data, size_t sizeInBytes)
{
   return runebqWrite(&stream->queueTx, data, sizeInBytes);
}

size_t runesRead(struct rune_stream * stream, void * data, size_t sizeInBytes)
{
   return runebqRead(&stream->queueRx, data, sizeInBytes);
}

static inline 
void runesMessageReceive(struct rune_stream * stream,
                         struct rune_bit_buffer * buffer)
{
   (void)stream;
   (void)buffer;
}

void runemsMessageReceive(struct rune_module_stream * mStream,
                          struct rune_bit_buffer * buffer)
{
   size_t i, count;
   struct rune_stream ** streams = runelGetAll_stream(&mStream->streamList, &count);

   for(i = 0; i < count; i ++)
   {
      runesMessageReceive(streams[i], buffer);
   } 
}

static inline
void runesMessageFill(struct rune_stream * stream, 
                      uint8_t id,
                      struct rune_bit_buffer * buffer)
{
   (void)stream;
   (void)id;
   (void)buffer;
}

void runemsMessageFill(struct rune_module_stream * mStream,
                       uint8_t id,
                       struct rune_bit_buffer * buffer)
{
   size_t i, count;
   struct rune_stream ** streams = runelGetAll_stream(&mStream->streamList, &count);

   for(i = 0; i < count; i ++)
   {
      runesMessageFill(streams[i], id, buffer);
   } 
}

static inline
void runesMessageNotification(struct rune_stream * stream,
                              uint8_t id,
                              enum rune_message_notification notification)
{
   (void)stream;
   (void)id;
   (void)notification;
}

void runemsMessageNotification(struct rune_module_stream * mStream,
                               uint8_t id, 
                               enum rune_message_notification notification)
{
   size_t i, count;
   struct rune_stream ** streams = runelGetAll_stream(&mStream->streamList, &count);

   for(i = 0; i < count; i ++)
   {
      runesMessageNotification(streams[i], id, notification);
   } 
}


