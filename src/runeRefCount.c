#include <runeRefCount.h>


void runercInit(struct rune_refcount * refcount)
{
   refcount->count = 1;
}

void runercTake(struct rune_refcount * refcount)
{
   refcount->count ++;
}

bool runercRelease(struct rune_refcount * refcount)
{
   if(refcount->count > 0)
   {
      refcount->count --;
   }

   return refcount->count == 0;
}

