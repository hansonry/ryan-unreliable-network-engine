#include <runeBitBuffer.h>

void runebbInitReader(struct rune_bit_buffer * bitBuffer, 
                      const void * buffer, size_t sizeInBytes)
{
   bitBuffer->buffer       = (uint8_t *)buffer;
   bitBuffer->bufferLength = sizeInBytes;
   bitBuffer->index.byte   = 0;
   bitBuffer->index.bit    = 0;
   bitBuffer->readOnly     = true;
}

void runebbInitWriter(struct rune_bit_buffer * bitBuffer, 
                      void * buffer, size_t sizeInBytes)
{
   bitBuffer->buffer       = buffer;
   bitBuffer->bufferLength = sizeInBytes;
   bitBuffer->index.byte   = 0;
   bitBuffer->index.bit    = 0;
   bitBuffer->readOnly     = false;
}

// Start
// |aaaaaa xx|xxxxxx xx|xxxxxx xx|
//        |bb bbbbbb|bb bbbbbb|

// Setup
// |aaaaaa 00|xxxxxx xx|xxxxxx xx|
//        |bb bbbbbb|bb bbbbbb|


// Step 1
// |aaaaaa bb|xxxxxx xx|xxxxxx 00|
//        |bb bbbbbb|bb bbbbbb|

// Step 2
// |aaaaaa bb|bbbbbb 00|xxxxxx 00|
//        |bb bbbbbb|bb bbbbbb|

static inline
size_t WriteBits(struct rune_bit_buffer * buffer,
                 const uint8_t * values, 
                 size_t elementCount, size_t elementSizeInBits)
{
   size_t elementsWritten = 0;
   size_t bitAcc = 0;
  
   // Setup
   const uint8_t bitIndex = buffer->index.bit;
   const uint8_t mask = 0xFF << bitIndex;
   const uint8_t shift1 = bitIndex;
   const uint8_t shift2 = 8 - bitIndex;
   uint8_t * u8Buffer = &buffer->buffer[buffer->index.byte];
   
   if(buffer->readOnly)
   {
      return 0;
   }
   
   (*u8Buffer) &= ~mask;
   
   while(elementsWritten < elementCount && 
         buffer->index.byte < buffer->bufferLength)
   {
      // Step 1
      (*u8Buffer) |= (*values) << shift1;
      buffer->index.byte ++;
      buffer->index.bit = 0;
      u8Buffer++;
      
   
      if(bitIndex != 0)
      {
         if(buffer->index.byte >= buffer->bufferLength)
         {
            bitAcc += 8 - bitIndex;
            break;
         }
      
         // Step 2
         (*u8Buffer) = (*values) >> shift2;
         buffer->index.bit = bitIndex;
      }
      values++;
      bitAcc += 8;
      
      if(bitAcc >= elementSizeInBits)
      {
         const size_t newElements = bitAcc / elementSizeInBits;
         bitAcc = bitAcc % elementSizeInBits;
         elementsWritten += newElements;
      }
   }

   if(bitAcc >= elementSizeInBits)
   {
      const size_t newElements = bitAcc / elementSizeInBits;
      bitAcc = bitAcc % elementSizeInBits;
      elementsWritten += newElements;
   }
   
   // Return bits back to the accumulator if we overshoot
   if(elementsWritten > elementCount)
   {
      bitAcc += (elementsWritten - elementCount) * elementSizeInBits;
      elementsWritten = elementCount;
   }
   
   // Ajust the pointer
   if(bitAcc > 0)
   {
      size_t bytes = bitAcc / 8;
      size_t bits = bitAcc % 8;
      buffer->index.byte -= bytes;
      if(buffer->index.bit < bits)
      {
         buffer->index.byte --;
         buffer->index.bit += (8 - bits);
      }
      else
      {
         buffer->index.bit -= bits;
      }
   }
   return elementsWritten;
}

size_t runebbWriteBits(struct rune_bit_buffer * buffer, 
                       const uint8_t * value, 
                       size_t elementCount, size_t elementSizeInBits)
{
   return WriteBits(buffer, value, elementCount, elementSizeInBits);
}

size_t runebbWriteBytes(struct rune_bit_buffer * buffer, 
                        const void * value, 
                        size_t elementCount, size_t elementSizeInBytes)
{
   return WriteBits(buffer, value, elementCount, elementSizeInBytes * 8);
}

size_t runebbWriteRemainingBits(struct rune_bit_buffer * buffer, bool value)
{
   
   if(buffer->index.bit > 0)
   {
      uint8_t bits = value ? 0xFF : 0x00;
      size_t bitsToWrite = 8 - buffer->index.bit;
      return WriteBits(buffer, &bits, bitsToWrite, 1);
   }
   return 0;
}

// Start
// |aaaaaa bb|bbbbbb bb|bbbbbb bb|
//        |xx xxxxxx|xx xxxxxx|xx xxxxxx|

// Step 1
// |aaaaaa bb|bbbbbb bb|bbbbbb bb|
//        |bb xxxxxx|xx xxxxxx|xx xxxxxx|

// Step 2
// |aaaaaa bb|bbbbbb bb|bbbbbb bb|
//        |bb bbbbbb|xx xxxxxx|xx xxxxxx|

static inline
size_t ReadBits(struct rune_bit_buffer * buffer, 
                uint8_t * values, size_t elementCount, size_t elementSizeInBits)
{
   size_t elementsRead = 0;
   size_t bitAcc = 0;
  
   // Setup
   const uint8_t bitIndex = buffer->index.bit;
   const uint8_t shift1 = bitIndex;
   const uint8_t shift2 = 8 - bitIndex;
   const uint8_t * u8Buffer = &buffer->buffer[buffer->index.byte];
      
   while(elementsRead < elementCount && 
         buffer->index.byte < buffer->bufferLength)
   {
      // Step 1
      (*values) = (*u8Buffer) >> shift1;
      buffer->index.byte ++;
      buffer->index.bit = 0;
      u8Buffer++;
      
   
      if(bitIndex != 0)
      {
         if(buffer->index.byte >= buffer->bufferLength)
         {
            bitAcc += 8 - bitIndex;
            break;
         }
      
         // Step 2
         (*values) |= (*u8Buffer) << shift2;
         buffer->index.bit = bitIndex;
      }
      values++;
      bitAcc += 8;
      
      if(bitAcc >= elementSizeInBits)
      {
         const size_t newElements = bitAcc / elementSizeInBits;
         bitAcc = bitAcc % elementSizeInBits;
         elementsRead += newElements;
      }
   }

   if(bitAcc >= elementSizeInBits)
   {
      const size_t newElements = bitAcc / elementSizeInBits;
      bitAcc = bitAcc % elementSizeInBits;
      elementsRead += newElements;
   }
   
   // Return bits back to the accumulator if we overshoot
   if(elementsRead > elementCount)
   {
      bitAcc += (elementsRead - elementCount) * elementSizeInBits;
      elementsRead = elementCount;
   }
   
   // Ajust the pointer
   if(bitAcc > 0)
   {
      size_t bytes = bitAcc / 8;
      size_t bits  = bitAcc % 8;
      buffer->index.byte -= bytes;
      if(buffer->index.bit < bits)
      {
         buffer->index.byte --;
         buffer->index.bit += (8 - bits);
      }
      else
      {
         buffer->index.bit -= bits;
      }
   }
   return elementsRead;
}

size_t runebbReadBits(struct rune_bit_buffer * buffer, 
                      uint8_t * value, 
                      size_t elementCount, size_t elementSizeInBits)
{
   return ReadBits(buffer, value, elementCount, elementSizeInBits);
}

size_t runebbReadBytes(struct rune_bit_buffer * buffer, 
                       void * value, 
                       size_t elementCount, size_t elementSizeInBytes)
{
   return ReadBits(buffer, value, elementCount, elementSizeInBytes * 8);
}


static inline
size_t runebbRead(struct rune_readable * readable, 
                    size_t count, size_t elementSize, 
                    void * data)
{
   struct runer_bit_buffer * bb = (struct runer_bit_buffer *)readable;
   return runebbReadBytes(bb->buffer, data, count, elementSize);
}

static inline
size_t runebbWrite(struct rune_writable * writable, 
                    size_t count, size_t elementSize, 
                    const void * data)
{
   struct runew_bit_buffer * bb = (struct runew_bit_buffer *)writable;
   return runebbWriteBytes(bb->buffer, data, count, elementSize);
}

const struct rune_readable_vt ReadableBitBufferVT =
{
   runebbRead
};

const struct rune_writable_vt WritableBitBufferVT = 
{
   runebbWrite
};


