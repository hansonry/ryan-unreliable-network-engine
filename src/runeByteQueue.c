#include <string.h>

#include <runeByteQueue.h>

#define FRAME_COUNT 2


static inline
void runebqfInit(struct runebq_frame * frame, size_t initSize, size_t growBy)
{
   runelInit_byte(&frame->byteList, initSize, growBy);
}

void runebqInit(struct rune_bytequeue * queue, size_t initSize, size_t growBy)
{
   size_t i;
   for(i = 0; i < FRAME_COUNT; i++)
   {
      runebqfInit(&queue->frames[i], initSize, growBy);
   }

   queue->back.frame = &queue->frames[0];
   queue->back.index = 0;
   queue->front.frame = &queue->frames[1];
   queue->front.index = 0; 
   queue->count = 0;
}

static inline
void runebqfCleanup(struct runebq_frame * frame)
{
   runelCleanup_byte(&frame->byteList);
}

void runebqCleanup(struct rune_bytequeue * queue)
{
   size_t i;
   for(i = 0; i < FRAME_COUNT; i++)
   {
      runebqfCleanup(&queue->frames[i]);
   }
}

size_t runebqWriteReadable(struct rune_bytequeue * queue, 
                           struct rune_readable * readable, size_t sizeInBytes)
{
   struct runebq_frame * frame = queue->back.frame;
   uint8_t * data = runelAddMany_byte(&frame->byteList, sizeInBytes, NULL);
   runerRead(readable, sizeInBytes, 1, data);
   queue->back.index += sizeInBytes;
   queue->count += sizeInBytes;
   return sizeInBytes;
}

static inline
size_t runebqfSpaceRemaining(const struct runebq_byteptr * ptr)
{
   return runelGetCount_byte(&ptr->frame->byteList) - ptr->index;
}

static inline
uint8_t * runebqGetPointer(const struct runebq_byteptr * ptr)
{
   return runelGetPtr_byte(&ptr->frame->byteList, ptr->index);
}

static inline
struct runebq_frame * runebqGetOtherFrame(struct rune_bytequeue * queue,
                                          struct runebq_frame * frame)
{
   return (frame == &queue->frames[0]) ? &queue->frames[1] : &queue->frames[0];
}

static inline
void runebqSwapBackIfNeeded(struct rune_bytequeue * queue)
{
   if(queue->front.frame == queue->back.frame)
   {
      queue->back.frame = runebqGetOtherFrame(queue, queue->back.frame);
      queue->back.index = 0;
      
      runelClear_byte(&queue->back.frame->byteList);
   }
}

static inline
size_t runebqReadFrame(struct rune_bytequeue * queue, 
                       struct runebq_byteptr * ptr, 
                       struct rune_writable * writable, size_t sizeInBytes)

{
   const size_t frameRemainingSize = runebqfSpaceRemaining(ptr);
   const uint8_t * bytes = runebqGetPointer(ptr);
   const size_t bytesToCopy = (frameRemainingSize >= sizeInBytes) ? 
                               sizeInBytes : frameRemainingSize;
   runewWrite(writable, bytesToCopy, 1, bytes);
   ptr->index += bytesToCopy;
   
   if(bytesToCopy == frameRemainingSize)
   {
      ptr->index = 0;
      ptr->frame = runebqGetOtherFrame(queue, ptr->frame);
   }

   return bytesToCopy;
}

static inline
size_t runebqReadFromPtr(struct rune_bytequeue * queue, 
                         struct runebq_byteptr * ptr, 
                         struct rune_writable * writable, size_t sizeInBytes)
{
   const bool inBackFrame = ptr->frame == queue->back.frame;
   size_t bytesCopied = runebqReadFrame(queue, ptr, writable, sizeInBytes);
   sizeInBytes -= bytesCopied;
   if(!inBackFrame && sizeInBytes > 0)
   {
      bytesCopied += runebqReadFrame(queue, ptr, writable, sizeInBytes);
   }

   return bytesCopied;
}

size_t runebqReadWritable(struct rune_bytequeue * queue, 
                          struct rune_writable * writable, size_t sizeInBytes)
{
   const size_t bytesCopied = runebqReadFromPtr(queue, &queue->front, 
                                                writable, sizeInBytes);
   runebqSwapBackIfNeeded(queue);
   queue->count -= bytesCopied;
   return bytesCopied;
}

static inline 
struct runebq_byteptr * runebqpCopy(struct runebq_byteptr * dest, 
                                    const struct runebq_byteptr * src)
{
   dest->frame = src->frame;
   dest->index = src->index;
   return dest;
}

size_t runebqPeekWritable(struct rune_bytequeue * queue,
                          const struct runebq_byteptr * start, 
                          struct rune_writable * writable, size_t sizeInBytes,
                          struct runebq_byteptr * end)
{
   struct runebq_byteptr tempPtr;
   const size_t bytesCopied = runebqReadFromPtr(queue, 
                                                runebqpCopy(&tempPtr, start), 
                                                writable, sizeInBytes);
   
   if(end != NULL)
   {
      (void)runebqpCopy(end, &tempPtr);   
   } 
   return bytesCopied;
}

