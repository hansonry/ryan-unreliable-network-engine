#include <runeRepeater.h>



void runemrepMessageFill(struct rune_repeater * repeater,
                         uint8_t id,
                         struct rune_bit_buffer * buffer)
{
   switch(repeater->state)
   {
   case eRUNEREP_WaitingToSend:
      repeater->vt->fpFillMessage(repeater, id, buffer);
      repeater->state = eRUNEREP_WaitingForResponse;
      repeater->id    = id;
      repeater->attempts ++;
      break;
   case eRUNEREP_WaitingForResponse:
      break;
   case eRUNEREP_Complete:
      break;
   case eRUNEREP_Failure:
      break;
   }
}

void runemrepMessageNotification(struct rune_repeater * repeater,
                                 uint8_t id, 
                                 enum rune_message_notification notification)
{
   switch(repeater->state)
   {
   case eRUNEREP_WaitingToSend:
      break;
   case eRUNEREP_WaitingForResponse:
      if(id == repeater->id)
      {
         if(notification == eRUNEMN_Received)
         {
            repeater->state = eRUNEREP_Complete;
            repeater->id = 0;
         }
         else
         {
            if(repeater->retryMax == -1 || 
               repeater->attempts < repeater->retryMax)
            {
               repeater->state = eRUNEREP_WaitingToSend;
            }
            else
            {
               repeater->state = eRUNEREP_Failure;
               repeater->id = 0;
            }
         }
         repeater->vt->fpNotification(repeater, id, notification);
      }
      break;
   case eRUNEREP_Complete:
      break;
   case eRUNEREP_Failure:
      break;
   }
}


