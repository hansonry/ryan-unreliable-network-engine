#include <string.h>
#include <runeReadable.h>


static inline
size_t runeraRead(struct rune_readable * readable, 
                  size_t count, size_t elementSize, 
                  void * data)
{
   struct runer_array * array = (struct runer_array *)readable;
   size_t bytesToRead = count * elementSize;
   memcpy(data, array->array, bytesToRead);
   array->array += bytesToRead;
   return bytesToRead;
}

const struct rune_readable_vt ReadableArrayVT =
{
   runeraRead
};
