#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <runeHashMap.h>

#define DEFAULT_BUCKET_COUNT 128

struct runehm_pair
{
   struct runehm_pair * next;
   unsigned char data[];
};

struct runehm_pair_lookup
{
   bool valid;
   struct runehm_pair * target;
   struct runehm_pair ** prevLink;
   size_t bucketIndex;
};

static inline
void * runehmPairGetKey(struct rune_hashmap * map, 
                        struct runehm_pair * pair)
{
   (void)map;
   return pair->data;
}

static inline
void * runehmPairGetValue(struct rune_hashmap * map, 
                          struct runehm_pair * pair)
{
   return &pair->data[map->keySize];
}

// Copied from https://sites.google.com/site/murmurhash/
static inline
uint32_t runehmHash(const void * key, size_t len, uint32_t seed)
{
	// 'm' and 'r' are mixing constants generated offline.
	// They're not really 'magic', they just happen to work well.

	const uint32_t m = 0x5bd1e995;
	const uint32_t r = 24;

	// Initialize the hash to a 'random' value

	uint32_t h = seed ^ len;

	// Mix 4 bytes at a time into the hash

	const uint8_t * data = (const uint8_t *)key;

	while(len >= 4)
	{
		uint32_t k = *(uint32_t *)data;

		k *= m; 
		k ^= k >> r; 
		k *= m; 
		
		h *= m; 
		h ^= k;

		data += 4;
		len -= 4;
	}
	
	// Handle the last few bytes of the input array

	switch(len)
	{
	case 3: h ^= data[2] << 16;
           // fall through
	case 2: h ^= data[1] << 8;
           // fall through
	case 1: h ^= data[0];
	        h *= m;
	};

	// Do a few final mixes of the hash to ensure the last few
	// bytes are well-incorporated.

	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;

	return h;
}

static inline
size_t runehmGetBucketIndex(struct rune_hashmap * map, 
                            const void * key)
{
   uint32_t hash = runehmHash(key, map->keySize, 0);
   return hash % map->bucketCount;
}

static inline
void runehmLookup(struct rune_hashmap * map, 
                  const void * key,
                  struct runehm_pair_lookup * location)
{
   size_t index = runehmGetBucketIndex(map, key);
   struct runehm_pair * link      = map->buckets[index];
   struct runehm_pair ** prevLink = &map->buckets[index];
   location->valid       = false;
   location->bucketIndex = index;
   while(location->valid == false && 
         link            != NULL)
   {
      const char * linkKey = runehmPairGetKey(map, link);
      if(memcmp(key, linkKey, map->keySize) == 0)
      {
         location->valid    = true;
         location->target   = link;
         location->prevLink = prevLink;
      }
      prevLink = &link->next;
      link     = link->next;
   }
}

void runehmInit(struct rune_hashmap * map, size_t keySize, size_t valueSize, 
                size_t bucketCount)
{
   if(bucketCount == 0)
   {
      map->bucketCount = DEFAULT_BUCKET_COUNT;
   }
   else
   {
      map->bucketCount = bucketCount;
   }

   map->keySize   = keySize;
   map->valueSize = valueSize;
   map->count     = 0;
   
   map->buckets = calloc(map->bucketCount, sizeof(struct runehm_pair *));
   
   // Maybe not nessary
   memset(map->buckets, 0, sizeof(struct runehm_pair *) * map->bucketCount);
}


void runehmCleanup(struct rune_hashmap * map)
{
   runehmClear(map);
   free(map->buckets);
}

static inline
struct runehm_pair * runehmCreateLink(struct rune_hashmap * map,
                                      struct runehm_pair * next,
                                      const void * key,
                                      const void * value)
{
   struct runehm_pair * newLink = malloc(sizeof(struct runehm_pair));
   void * linkKey   = runehmPairGetKey(map,   newLink);
   void * linkValue = runehmPairGetValue(map, newLink);
   newLink->next    = next;
   memcpy(linkKey,   key,   map->keySize);
   memcpy(linkValue, value, map->valueSize);
   return newLink;
}


void runehmPut(struct rune_hashmap * map, const void * key, const void * value)
{
   struct runehm_pair_lookup lookup;
   runehmLookup(map, key, &lookup);
   if(lookup.valid)
   {
      void * targetValue = runehmPairGetValue(map, lookup.target);
      memcpy(targetValue, value, map->valueSize);
   }
   else
   {
      struct runehm_pair ** bucketPtr = &map->buckets[lookup.bucketIndex];
      (*bucketPtr) = runehmCreateLink(map, *bucketPtr, key, value);
      map->count ++;
   }
}

bool runehmGet(struct rune_hashmap * map, void * value, const void * key)
{
   struct runehm_pair_lookup lookup;
   runehmLookup(map, key, &lookup);
   if(lookup.valid)
   {
      const void * targetValue = runehmPairGetValue(map, lookup.target);
      memcpy(value, targetValue, map->valueSize);
   }
   return lookup.valid;
}

bool runehmHasKey(const struct rune_hashmap * map, const void * key)
{
   struct runehm_pair_lookup lookup;
   runehmLookup((struct rune_hashmap *)map, key, &lookup);
   return lookup.valid;
}


bool runehmRemove(struct rune_hashmap * map, const void * key)
{
   struct runehm_pair_lookup lookup;
   runehmLookup(map, key, &lookup);
   if(lookup.valid)
   {
      (*lookup.prevLink) = lookup.target->next;
      free(lookup.target);
      map->count --;
   }
   return lookup.valid;
}

static inline
void runehmClearBucket(struct runehm_pair * bucket)
{
   struct runehm_pair * temp;
   while(bucket != NULL)
   {
      temp = bucket;
      bucket = bucket->next;
      free(temp);
   }
}

void runehmClear(struct rune_hashmap * map)
{
   size_t i;
   for(i = 0; i < map->bucketCount; i++)
   {
      runehmClearBucket(map->buckets[i]);
      map->buckets[i] = NULL;
   }
   map->count = 0;
}
