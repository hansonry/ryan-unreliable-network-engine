#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <runeList.h>

void runelInit(struct rune_list * list, size_t elementSize, 
               size_t initSize, size_t growBy)
{
   list->elementSize = elementSize;
   list->growBy = (growBy != 0) ? growBy : 32;
   list->count = 0;
   list->size = (initSize != 0) ? initSize : list->growBy;

   list->base = calloc(list->size, list->elementSize);
   
}

void runelCleanup(struct rune_list * list)
{
   free(list->base);
   list->elementSize = 0;
   list->count = 0;
   list->size = 0;
   list->base = 0;
   list->growBy = 0;
}


static inline
void MakeRoomFor(struct rune_list * list, size_t count)
{
   if(count > list->size)
   {
      size_t newSize = count + list->growBy;
      list->base = realloc(list->base, newSize * list->elementSize);
      list->size = newSize;
   }
}

static inline
void * GetAddressOf(const struct rune_list * list, size_t index)
{
   uint8_t * u8Base = list->base;
   return &u8Base[index * list->elementSize];
}

static inline
void ElementCopy(void * dest, const void * src, size_t count, 
                 const struct rune_list * list)
{
   memcpy(dest, src, list->elementSize * count);
}

void * runelAdd(struct rune_list * list, size_t count, size_t * atIndex)
{
   size_t index = list->count;
   void * item;   
   MakeRoomFor(list, list->count + count);
   
   item = GetAddressOf(list, index);

   list->count += count;
   if(atIndex != NULL)
   {
      (*atIndex) = index;
   }
   return item;
}


void * runelAddCopy(struct rune_list * list, const void * item, size_t count,
                    size_t * atIndex)
{
   void * newItem = runelAdd(list, count, atIndex);
   ElementCopy(newItem, item, count, list);
   return newItem;
}

void runelClear(struct rune_list * list)
{
   list->count = 0;
}

static inline
void Swap(struct rune_list * list, size_t index1, size_t index2)
{
   void * temp;
   void * value1 = GetAddressOf(list, index1);
   void * value2 = GetAddressOf(list, index2);
   // Make Room for temp
   MakeRoomFor(list, list->count + 1);
   temp = GetAddressOf(list, list->count);

   memcpy(temp,   value1, list->elementSize);
   memcpy(value1, value2, list->elementSize);
   memcpy(value2, temp,   list->elementSize);
}

static inline
void RemoveFast(struct rune_list * list, size_t index)
{
   size_t lastIndex = list->count - 1;
   if(lastIndex != index)
   {
      Swap(list, lastIndex, index);
   }
   list->count --;
}

bool runelRemoveFast(struct rune_list * list, size_t index)
{
   if(index >= list->count)
   {
      return false;
   }
   
   RemoveFast(list, index);
   return true;
}

static inline
void RemoveOrdered(struct rune_list * list, size_t index)
{
   const size_t startCopy = index + 1;
   size_t i;
   for(i = startCopy; i < list->count; i++)
   {
      void * dest = GetAddressOf(list, i - 1);
      void * src  = GetAddressOf(list, i);
      memcpy(dest, src, list->elementSize);
   }
   list->count --;
}

bool runelRemoveOrdered(struct rune_list * list, size_t index)
{
   if(index >= list->count)
   {
      return false;
   }
   
   RemoveOrdered(list, index);
   return true;
}


