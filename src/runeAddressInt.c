#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <runeAddressInt.h>

#define DECLARE_CAST(type, name, value) \
type * name = (type *)value


static 
int runeAddressIntCompare(const struct rune_address * a, 
                          const struct rune_address * b)
{
   DECLARE_CAST(const struct rune_address_int, ai, a);
   DECLARE_CAST(const struct rune_address_int, bi, b);
   return ai->address - bi->address;
}

static
const char * runeAddressIntGetText(const struct rune_address * address)
{
   DECLARE_CAST(const struct rune_address_int, intAddr, address);
   return intAddr->text;
}

static
void runeAddressIntFree(struct rune_address * address)
{
   DECLARE_CAST(struct rune_address_int, intAddr, address);
   free(intAddr->text);
   free(intAddr);
}


static const struct rune_address_vt runeAddressVT_Int = {
   runeAddressIntCompare,
   runeAddressIntGetText,
   runeAddressIntFree
};


static inline
char * CreateStringFromInt(int i)
{
   int size = snprintf(NULL, 0, "%d", i) + 1;
   char * str = malloc(size);
   snprintf(str, size, "%d", i);
   return str;
}


struct rune_address * runeAddressIntNew(int address)
{
   struct rune_address_int * intAddr = malloc(sizeof(struct rune_address_int));

   intAddr->address = address;
   intAddr->text    = CreateStringFromInt(intAddr->address);
   
   return runeAddressInit(&intAddr->base, &runeAddressVT_Int);
}


