#include <stdlib.h>
#include <runeList.h>
#include <runeConnectionManager.h>
#include <runeRefCount.h>

RUNE_LIST_TYPE_CREATE(msgid, uint8_t)

#define DEFAULT_RATE_TIMEOUT_MS  250
#define DEFAULT_RATE_SIZE        512

struct runec_rate
{
   int timeout_ms;
   size_t size;
};

struct rune_connection
{
   struct rune_address * address;
   struct rune_connection_manager * manager;
   struct rune_refcount refCount;
   unsigned int timeSinceLastReceive_ms;
   struct rune_connection_settings settings;
   enum rune_connection_state currentState;
   int messageSendTimer_ms;
   struct runec_rate rateOut;
   struct runec_rate rateIn;
   struct runec_rate rateInDesired;
   uint8_t * messageBuffer;
   size_t messageBufferSize;
   uint8_t nextId;
   uint8_t lastReceivedId;
   struct rune_list_msgid receivedIds;
   struct rune_list_msgid sentIds;
   size_t maxSentWindowSize;
};

RUNE_LIST_TYPE_CREATE(con, struct rune_connection * )

struct rune_connection_manager
{
   struct rune_list_con connections;
   struct rune_refcount refCount;
   void * userData;
   NewConnection_FP newConnectionCallback;
   SendMessage_FP sendMessageCallback;
};

struct rune_connection_manager * runecmNew(void * userData,
                                           SendMessage_FP sendMessageCallback,
                                           NewConnection_FP newConnectionCallback)
{
   struct rune_connection_manager * manager = 
                                malloc(sizeof(struct rune_connection_manager));

   manager->userData = userData;
   manager->newConnectionCallback = newConnectionCallback;
   manager->sendMessageCallback = sendMessageCallback;
   runelInit_con(&manager->connections, 0, 0);
   runercInit(&manager->refCount);

   return manager;
}

void runecmTake(struct rune_connection_manager * manager)
{
   runercTake(&manager->refCount);
}

void runecmRelease(struct rune_connection_manager * manager)
{
   if(runercRelease(&manager->refCount))
   {
      size_t count, i;
      struct rune_connection ** connections = runelGetAll_con(&manager->connections,
                                                              &count);
      for(i = 0; i < count; i++)
      {
         runecRelease(connections[i]);
      } 
      runelCleanup_con(&manager->connections);
      free(manager);
   }
}

static inline
struct rune_connection * runecNew(struct rune_connection_manager * manager,
                                  struct rune_address * address, 
                                  bool incoming)
{
   struct rune_connection * connection = malloc(sizeof(struct rune_connection));
   runercInit(&connection->refCount);
   connection->address = address;
   connection->manager = manager;
   connection->timeSinceLastReceive_ms = 0;
   connection->currentState = incoming ? eRUNECS_Connected : eRUNECS_Connecting;
   connection->rateOut.size             = DEFAULT_RATE_SIZE;
   connection->rateOut.timeout_ms       = DEFAULT_RATE_TIMEOUT_MS;
   connection->rateIn.size              = 0;
   connection->rateIn.timeout_ms        = 0;
   connection->rateInDesired.size       = DEFAULT_RATE_SIZE;
   connection->rateInDesired.timeout_ms = DEFAULT_RATE_TIMEOUT_MS;
   
   connection->messageSendTimer_ms = 0;
   connection->nextId = 0;
   connection->lastReceivedId = 255;
   connection->maxSentWindowSize = 3;
   
   connection->settings.messageReceivedCallback     = NULL;
   connection->settings.messageFillCallback         = NULL;
   connection->settings.messageNotificationCallback = NULL;
   connection->settings.userData = NULL;
   
   runelInit_msgid(&connection->receivedIds, 0, 0);
   runelInit_msgid(&connection->sentIds, 0, 0);
   connection->messageBufferSize = connection->rateOut.size;
   connection->messageBuffer = malloc(connection->messageBufferSize);
   runeAddressTake(connection->address);
   return connection;
}

static inline
void runecSetMessageSize(struct rune_connection * connection,
                         size_t newMessageSize)
{
   connection->rateOut.size = newMessageSize;
   if(connection->messageBufferSize < newMessageSize)
   {
      connection->messageBuffer = realloc(connection->messageBuffer, 
                                          newMessageSize);
      connection->messageBufferSize = newMessageSize;
   }
}

void runecTake(struct rune_connection * connection)
{
   runercTake(&connection->refCount);
}

void runecRelease(struct rune_connection * connection)
{
   if(runercRelease(&connection->refCount))
   {
      runeAddressRelease(connection->address);
      runelCleanup_msgid(&connection->receivedIds);
      runelCleanup_msgid(&connection->sentIds);
      free(connection->messageBuffer);
      free(connection);
   }
}


static inline
struct rune_connection * runecmFindConnectionWithAddress(struct rune_connection_manager * manager,
                                                         const struct rune_address * address)
{
   size_t count, i;
   struct rune_connection ** connections = runelGetAll_con(&manager->connections,
                                                           &count);
   for(i = 0; i < count; i++)
   {
      if(runeAddressCompare(address, connections[i]->address) == 0)
      {
         return connections[i];
      }
   } 

   return NULL;
}

static inline 
bool IsMessageIdLargerThan(uint8_t a, uint8_t b)
{
   if(a > b)
   {
      uint8_t diff = a - b;
      return diff < 128;
   }
   else
   {
      uint8_t diff = b - a;
      return diff >= 128;
   }
}

static inline
void runecNotify(struct rune_connection * connection,
                 uint8_t id,
                 enum rune_message_notification notification)
{
   if(connection->settings.messageNotificationCallback != NULL)
   {
      connection->settings.messageNotificationCallback(connection,
                                                       id,
                                                       notification,
                                                       connection->settings.userData);
   }
}

static inline
void runecUpdateWindow(struct rune_connection * connection,
                       uint8_t id)
{
   size_t i, count;
   uint8_t * sentIds;
   
   sentIds = runelGetAll_msgid(&connection->sentIds, &count);
   
   for(i = 0; i < count; i++)
   {
      if(id == sentIds[i])
      {
         runecNotify(connection, sentIds[i], eRUNEMN_Received);
      }
      else if(IsMessageIdLargerThan(id, sentIds[i]))
      {
         runecNotify(connection, sentIds[i], eRUNEMN_Dropped);
      }
   }
   
   for(i = count - 1; i < count; i--)
   {

      if(id == sentIds[i] ||
         IsMessageIdLargerThan(id, sentIds[i]))
      {
         runelRemoveOrdered_msgid(&connection->sentIds, i);
      }
      
   }
}

static inline
void runecParseMessage(struct rune_connection * connection,
                       struct rune_bit_buffer * buffer)
{
   uint8_t messageId;
   bool moreDataFlag;
   
   runebbReadByte(buffer, &messageId);
   
   runelAddCopy_msgid(&connection->receivedIds, messageId, NULL);
   
   runebbReadBit(buffer, &moreDataFlag);
   while(moreDataFlag)
   {
      uint8_t acknowledgedMessageId;
      runebbReadByte(buffer, &acknowledgedMessageId);
   
      runecUpdateWindow(connection, acknowledgedMessageId);
   
      runebbReadBit(buffer, &moreDataFlag);
   }
   
}

void runecReceiveMessage(struct rune_connection * connection,
                         const void * buffer, size_t bufferSize)
{
   struct rune_bit_buffer bitBuffer;
   runebbInitReader(&bitBuffer, buffer, bufferSize);
   
   runecParseMessage(connection, &bitBuffer);

   if(connection->settings.messageReceivedCallback != NULL)
   {
      connection->settings.messageReceivedCallback(connection, &bitBuffer,
                                                   connection->settings.userData); 
   }
}

static inline
struct rune_connection * runecmCreateAndAddConnection(struct rune_connection_manager * manager,
                                                      struct rune_address * address, 
                                                      bool incoming)
{
   struct rune_connection * connection = runecNew(manager, address, incoming);
   runelAddCopy_con(&manager->connections, connection, NULL);

   if(manager->newConnectionCallback != NULL)
   {
      manager->newConnectionCallback(manager, connection, 
                                     &connection->settings);
   }
   return connection;
}

void runecmReceiveMessage(struct rune_connection_manager * manager, 
                          const void * buffer, size_t bufferSize, 
                          struct rune_address * address)
{
   struct rune_connection * connection = 
                             runecmFindConnectionWithAddress(manager, address);
   if(connection == NULL)
   {
      // Connection not found. So new connection
      connection = runecmCreateAndAddConnection(manager, address, true);
   }
   runecReceiveMessage(connection, buffer, bufferSize); 
}

static inline
void runecStartMessage(struct rune_connection * connection, 
                       uint8_t id,
                       struct rune_bit_buffer * buffer)
{
   size_t i, count;
   uint8_t * receivedIds;
   
   // Write ID
   runebbWriteByte(buffer, id);
   
   // Write Received ID
   receivedIds = runelGetAll_msgid(&connection->receivedIds, &count);
   for(i = 0; i < count; i++)
   {
      runebbWriteBit(buffer, true);
      runebbWriteByte(buffer, receivedIds[i]);
   }
   
   runelClear_msgid(&connection->receivedIds);
   runebbWriteBit(buffer, false);
}

static inline
void runecBuildAndSendMessage(struct rune_connection * connection)
{
   struct rune_bit_buffer buffer; 
   size_t bytesWritten;
   const uint8_t id = connection->nextId;
   runebbInitWriter(&buffer, connection->messageBuffer, 
                             connection->rateOut.size);

   runecStartMessage(connection, id, &buffer);
   connection->nextId++;
   
   if(connection->settings.messageFillCallback != NULL)
   {
      connection->settings.messageFillCallback(connection, id, &buffer,
                                               connection->settings.userData);
   }
   // Finalize message
   runebbWriteRemainingBits(&buffer, false);

   // Send Message
   runelAddCopy_msgid(&connection->sentIds, id, NULL);
   
   bytesWritten = runebbGetBytesWritten(&buffer);
   connection->manager->sendMessageCallback(connection->manager,
                                            connection,
                                            connection->messageBuffer,
                                            bytesWritten,
                                            connection->address);
}

static inline
bool runecCanSendMore(const struct rune_connection * connection)
{
   return runelGetCount_msgid(&connection->sentIds) < 
          connection->maxSentWindowSize;
}

static inline
void runecUpdate(struct rune_connection * connection, int dt_ms)
{
   connection->messageSendTimer_ms -= dt_ms;
   while(connection->messageSendTimer_ms <= 0 &&
         runecCanSendMore(connection))
   {
      runecBuildAndSendMessage(connection);
      connection->messageSendTimer_ms += connection->rateOut.timeout_ms;
   }
}

void runecmUpdate(struct rune_connection_manager * manager, int dt_ms)
{
   size_t count, i;
   struct rune_connection ** connections = runelGetAll_con(&manager->connections,
                                                           &count);
   for(i = 0; i < count; i++)
   {
      runecUpdate(connections[i], dt_ms);
   } 
}

struct rune_connection * runecmConnectTo(struct rune_connection_manager * manager, 
                                         struct rune_address * address)
{
   struct rune_connection * connection = runecmCreateAndAddConnection(manager, 
                                                                      address, 
                                                                      false);

   runecBuildAndSendMessage(connection);
   return connection;
}

#ifdef RUNE_TESTING
#include <rmsymboltable.h>
RMM_SYMBOLTABLE_BEGIN(ConnectionManager)
   RMM_SYMBOLTABLE_FUNCTION(runecBuildAndSendMessage)
RMM_SYMBOLTABLE_END()
#endif // RUNE_TESTING

