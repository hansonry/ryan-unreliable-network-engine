#ifndef __RUNEHASHMAP_H__
#define __RUNEHASHMAP_H__
#include <stddef.h>
#include <stdbool.h>

struct runehm_pair;

struct rune_hashmap
{
   size_t keySize;
   size_t valueSize;
   size_t count;
   size_t bucketCount;
   struct runehm_pair ** buckets;
};

void runehmInit(struct rune_hashmap * map, size_t keySize, size_t valueSize, 
                size_t bucketCount);

void runehmCleanup(struct rune_hashmap * map);

void runehmPut(struct rune_hashmap * map, const void * key, const void * value);

bool runehmGet(struct rune_hashmap * map, void * value, const void * key);

bool runehmHasKey(const struct rune_hashmap * map, const void * key);

bool runehmRemove(struct rune_hashmap * map, const void * key);

void runehmClear(struct rune_hashmap * map);

#define RUNE_HASHMAP_TYPE_CREATE(name, keyType, valueType)                     \
struct rune_hashmap_##name                                                     \
{                                                                              \
   struct rune_hashmap map;                                                    \
};                                                                             \
                                                                               \
static inline                                                                  \
void runehmInit_##name (struct rune_hashmap_##name * map, size_t bucketCount)  \
{                                                                              \
   runehmInit(&map->map, sizeof(keyType), sizeof(valueType), bucketCount);     \
}                                                                              \
                                                                               \
static inline                                                                  \
void runehmCleanup_##name (struct rune_hashmap_##name * map)                   \
{                                                                              \
   runehmCleanup(&map->map);                                                   \
}                                                                              \
                                                                               \
static inline                                                                  \
void runehmPut_##name (struct rune_hashmap_##name * map,                       \
                       keyType key, valueType value)                           \
{                                                                              \
   runehmPut(&map->map, &key, &value);                                         \
}                                                                              \
                                                                               \
static inline                                                                  \
valueType runehmGet_##name (struct rune_hashmap_##name * map,                  \
                            keyType key)                                       \
{                                                                              \
   valueType value;                                                            \
   runehmGet(&map->map, &value, &key);                                         \
   return value;                                                               \
}                                                                              \
                                                                               \
static inline                                                                  \
bool runehmHasKey_##name (const struct rune_hashmap_##name * map,              \
                          keyType key)                                         \
{                                                                              \
   return runehmHasKey(&map->map, &key);                                       \
}                                                                              \
                                                                               \
static inline                                                                  \
bool runehmRemove_##name (struct rune_hashmap_##name * map,                    \
                          keyType key)                                         \
{                                                                              \
   return runehmRemove(&map->map, &key);                                       \
}                                                                              \
                                                                               \
static inline                                                                  \
bool runehmClear_##name (struct rune_hashmap_##name * map)                     \
{                                                                              \
   runehmClear(&map->map);                                                     \
}                                                                              \
                                                                               \
static inline                                                                  \
size_t runehmCount_##name (const struct rune_hashmap_##name * map)             \
{                                                                              \
   return map->map.count;                                                      \
}                                                                              \


#endif // __RUNEHASHMAP_H__

