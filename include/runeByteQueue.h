#ifndef __RUNEBYTEQUEUE_H__
#define __RUNEBYTEQUEUE_H__
#include <stdint.h>

#include <runeList.h>
#include <runeReadable.h>
#include <runeWritable.h>


RUNE_LIST_TYPE_CREATE(byte, uint8_t)

struct runebq_frame
{
   struct rune_list_byte byteList;
};

struct runebq_byteptr
{
   struct runebq_frame * frame;
   size_t index;
};

struct rune_bytequeue
{
   struct runebq_frame frames[2];
   struct runebq_byteptr front;
   struct runebq_byteptr back;
   size_t count;
};

void runebqInit(struct rune_bytequeue * queue, size_t initSize, size_t growBy);
void runebqCleanup(struct rune_bytequeue * queue);

size_t runebqWriteReadable(struct rune_bytequeue * queue, 
                           struct rune_readable * readable, size_t sizeInBytes);
size_t runebqReadWritable(struct rune_bytequeue * queue, 
                          struct rune_writable * writable, size_t sizeInBytes);

size_t runebqPeekWritable(struct rune_bytequeue * queue,
                          const struct runebq_byteptr * start, 
                          struct rune_writable * writable, size_t sizeInBytes,
                          struct runebq_byteptr * end);

static inline
size_t runebqCount(const struct rune_bytequeue * queue)
{
   return queue->count;
}

static inline
size_t runebqWrite(struct rune_bytequeue * queue, 
                   const void * data, size_t sizeInBytes)
{
   struct runer_array readable;
   return runebqWriteReadable(queue, runerInit(&readable, data), sizeInBytes);
}

static inline
size_t runebqRead(struct rune_bytequeue * queue, 
                  void * data, size_t sizeInBytes)
{
   struct runew_array writable;
   return runebqReadWritable(queue, runewInit(&writable, data), sizeInBytes);
}

static inline
size_t runebqPeek(struct rune_bytequeue * queue,
                  const struct runebq_byteptr * start, 
                  void * data, size_t sizeInBytes,
                  struct runebq_byteptr * end)
{
   struct runew_array writable;
   return runebqPeekWritable(queue, start, 
                             runewInit(&writable, data), sizeInBytes, 
                             end);
}


static inline
size_t runebqPeekFromFront(struct rune_bytequeue * queue,
                           void * data, size_t sizeInBytes,
                           struct runebq_byteptr * end)
{
   return runebqPeek(queue, &queue->front, data, sizeInBytes, end);
}

#endif // __RUNEBYTEQUEUE_H__


