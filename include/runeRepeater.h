#ifndef __RUNEREPEATER_H__
#define __RUNEREPEATER_H__
#include "runeBitBuffer.h"
#include "runeConnectionManager.h"

struct rune_repeater;

struct rune_repeater_vt
{
   void (*fpFillMessage)(struct rune_repeater * repeater,
                         uint8_t id, 
                         struct rune_bit_buffer * buffer);
   void (*fpNotification)(struct rune_repeater * repeater, 
                          uint8_t id,
                          enum rune_message_notification notification);
};

enum rune_repeater_state
{
   eRUNEREP_WaitingToSend,
   eRUNEREP_WaitingForResponse,
   eRUNEREP_Complete,
   eRUNEREP_Failure
};

struct rune_repeater
{
   const struct rune_repeater_vt * vt;
   int retryMax;
   int attempts;
   uint8_t id;
   enum rune_repeater_state state;
   
};

static inline
void runerepReset(struct rune_repeater * repeater)
{
   repeater->attempts = 0;
   repeater->id       = 0;
   repeater->state    = eRUNEREP_WaitingToSend;
}

static inline
struct rune_repeater * runerepInit(struct rune_repeater * repeater,
                                   const struct rune_repeater_vt * vt,
                                   int retries)
{
   repeater->vt = vt;
   repeater->retryMax = retries;
   runerepReset(repeater);

   return repeater;
}


void runemrepMessageFill(struct rune_repeater * repeater,
                         uint8_t id,
                         struct rune_bit_buffer * buffer);
void runemrepMessageNotification(struct rune_repeater * repeater,
                                 uint8_t id, 
                                 enum rune_message_notification notification);

#endif // __RUNEREPEATER_H__

