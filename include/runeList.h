#ifndef __RUNELIST_H__
#define __RUNELIST_H__
#include <stddef.h>
#include <stdbool.h>

struct rune_list
{
   void * base;
   size_t elementSize;
   size_t growBy;
   size_t size;
   size_t count;
};

void runelInit(struct rune_list * list, size_t elementSize, 
               size_t initSize, size_t growBy);
void runelCleanup(struct rune_list * list);


void * runelAdd(struct rune_list * list, size_t count, size_t * atIndex);
void * runelAddCopy(struct rune_list * list, const void * item, size_t count,
                    size_t * atIndex);


void runelClear(struct rune_list * list);

bool runelRemoveFast(struct rune_list * list, size_t index);
bool runelRemoveOrdered(struct rune_list * list, size_t index);


#define RUNE_LIST_TYPE_CREATE(name, type)                                     \
struct rune_list_## name                                                      \
{                                                                             \
   struct rune_list list;                                                     \
};                                                                            \
                                                                              \
static inline                                                                 \
void runelInit_ ## name (struct rune_list_ ## name * list,                    \
                         size_t initSize, size_t growBy)                      \
{                                                                             \
   runelInit(&list->list, sizeof(type), initSize, growBy);                    \
}                                                                             \
                                                                              \
static inline                                                                 \
void runelCleanup_ ## name (struct rune_list_ ## name * list)                 \
{                                                                             \
   runelCleanup(&list->list);                                                 \
}                                                                             \
                                                                              \
static inline                                                                 \
type * runelAdd_ ## name (struct rune_list_ ## name * list, size_t * atIndex) \
{                                                                             \
   return runelAdd(&list->list, 1, atIndex);                                  \
}                                                                             \
                                                                              \
static inline                                                                 \
type * runelAddMany_ ## name (struct rune_list_ ## name * list,               \
                              size_t count,                                   \
                              size_t * atIndex)                               \
{                                                                             \
   return runelAdd(&list->list, count, atIndex);                              \
}                                                                             \
                                                                              \
static inline                                                                 \
type * runelAddCopy_ ## name (struct rune_list_ ## name * list,               \
                              type item, size_t * atIndex)                    \
{                                                                             \
   return runelAddCopy(&list->list, &item, 1, atIndex);                       \
}                                                                             \
                                                                              \
static inline                                                                 \
type * runelAddCopyMany_ ## name (struct rune_list_ ## name * list,           \
                                  const type * items, size_t count,           \
                                  size_t * atIndex)                           \
{                                                                             \
   return runelAddCopy(&list->list, items, count, atIndex);                   \
}                                                                             \
                                                                              \
static inline                                                                 \
void runelClear_ ## name (struct rune_list_ ## name * list)                   \
{                                                                             \
   runelClear(&list->list);                                                   \
}                                                                             \
                                                                              \
static inline                                                                 \
bool runelRemoveFast_ ## name (struct rune_list_ ## name * list,              \
                               size_t index)                                  \
{                                                                             \
   return runelRemoveFast(&list->list, index);                                \
}                                                                             \
                                                                              \
static inline                                                                 \
bool runelRemoveOrdered_ ## name (struct rune_list_ ## name * list,           \
                                  size_t index)                               \
{                                                                             \
   return runelRemoveOrdered(&list->list, index);                             \
}                                                                             \
                                                                              \
static inline                                                                 \
size_t runelGetCount_ ## name (const struct rune_list_ ## name * list)        \
{                                                                             \
   return list->list.count;                                                   \
}                                                                             \
                                                                              \
static inline                                                                 \
type * runelGetPtr_ ## name (const struct rune_list_ ## name * list,          \
                             size_t index)                                    \
{                                                                             \
   const unsigned char * u8Base = list->list.base;                            \
   if(index >= list->list.count) return NULL;                                 \
   return (type *)&u8Base[list->list.elementSize * index];                            \
}                                                                             \
                                                                              \
static inline                                                                 \
type runelGetValue_ ## name (const struct rune_list_ ## name * list,          \
                             size_t index)                                    \
{                                                                             \
   type * item = runelGetPtr_ ## name (list, index);                          \
   return *item;                                                              \
}                                                                             \
                                                                              \
static inline                                                                 \
type * runelGetAll_ ## name (const struct rune_list_ ## name * list,          \
                             size_t * count)                                  \
{                                                                             \
   if(count != NULL) (*count) = list->list.count;                             \
   return list->list.base;                                                    \
}

#endif // __RUNELIST_H__

