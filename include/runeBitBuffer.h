#ifndef __RUNEBITBUFFER_H__
#define __RUNEBITBUFFER_H__
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include <runeReadable.h>
#include <runeWritable.h>

struct rune_bit_index
{
   size_t byte;
   uint8_t bit;
};

struct rune_bit_buffer
{
   uint8_t * buffer;
   size_t bufferLength;
   struct rune_bit_index index;
   bool readOnly;
};


void runebbInitReader(struct rune_bit_buffer * bitBuffer, 
                      const void * buffer, size_t sizeInBytes);

void runebbInitWriter(struct rune_bit_buffer * bitBuffer, 
                      void * buffer, size_t sizeInBytes);

size_t runebbWriteBits(struct rune_bit_buffer * buffer, 
                       const uint8_t * value, 
                       size_t elementCount, size_t elementSizeInBits);
size_t runebbWriteBytes(struct rune_bit_buffer * buffer, 
                        const void * value, 
                        size_t elementCount, size_t elementSizeInBytes);

size_t runebbWriteRemainingBits(struct rune_bit_buffer * buffer, bool value);

size_t runebbReadBits(struct rune_bit_buffer * buffer, 
                      uint8_t * value, 
                      size_t elementCount, size_t elementSizeInBits);
size_t runebbReadBytes(struct rune_bit_buffer * buffer, 
                       void * value, 
                       size_t elementCount, size_t elementSizeInBytes);

static inline
size_t runebbGetBytesWritten(const struct rune_bit_buffer * buffer)
{
   if(buffer->index.bit > 0)
   {
      return buffer->index.byte + 1;
   }
   return buffer->index.byte;
}


static inline
size_t runebbWriteBit(struct rune_bit_buffer * buffer, bool value)
{
   const uint8_t u8Value = value ? 0x01 : 0x00;
   return runebbWriteBits(buffer, &u8Value, 1, 1);
}

static inline
size_t runebbReadBit(struct rune_bit_buffer * buffer, bool * value)
{
   uint8_t u8Value = 0;
   size_t bitsRead = runebbReadBits(buffer, &u8Value, 1, 1);
   (*value) = (u8Value & 0x01) == 0x01;
   return bitsRead;
}

static inline
size_t runebbWriteByte(struct rune_bit_buffer * buffer, uint8_t value)
{
   return runebbWriteBytes(buffer, &value, 1, 1);
}

static inline
size_t runebbReadByte(struct rune_bit_buffer * buffer, uint8_t * value)
{
   return runebbReadBytes(buffer, value, 1, 1);
}

static inline
void runebiCopy(struct rune_bit_index * dest, 
                const struct rune_bit_index * src)
{
   dest->byte = src->byte;
   dest->bit  = src->bit;
}

static inline
void runebbReset(struct rune_bit_buffer * buffer)
{
   buffer->index.byte = 0;
   buffer->index.bit  = 0;
}


// Readable and Writable

struct runer_bit_buffer
{
   struct rune_readable parent;
   struct rune_bit_buffer * buffer;
};

struct runew_bit_buffer
{
   struct rune_writable parent;
   struct rune_bit_buffer * buffer;
};

extern const struct rune_readable_vt ReadableBitBufferVT;
extern const struct rune_writable_vt WritableBitBufferVT;

static inline
struct rune_readable * runebbrInit(struct runer_bit_buffer * readable, 
                                   struct rune_bit_buffer * buffer) 
{
   readable->parent.vt = &ReadableBitBufferVT;
   readable->buffer = buffer;
   return &readable->parent;
}

static inline
struct rune_writable * runebbwInit(struct runew_bit_buffer * writable, 
                                   struct rune_bit_buffer * buffer) 
{
   writable->parent.vt = &WritableBitBufferVT;
   writable->buffer = buffer;
   return &writable->parent;
}


#endif // __RUNEBITBUFFER_H__
