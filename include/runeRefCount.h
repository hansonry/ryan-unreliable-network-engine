#ifndef __RUNEREFCOUNT_H__
#define __RUNEREFCOUNT_H__
#include <stdbool.h>


struct rune_refcount
{
   unsigned int count;
};

void runercInit(struct rune_refcount * refcount);

void runercTake(struct rune_refcount * refcount);

bool runercRelease(struct rune_refcount * refcount);


#endif // __RUNEREFCOUNT_H__


