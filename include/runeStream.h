#ifndef __RUNESTREAM_H__
#define __RUNESTREAM_H__

#include "runeConnectionManager.h"

typedef unsigned char streamId_t;

struct rune_stream;

struct rune_module_stream;

struct rune_module_stream * runemsNew(struct rune_connection * connection);


void runemsTake(struct rune_module_stream * mStream);

void runemsRelease(struct rune_module_stream * mStream);


struct rune_stream * runemsGetStream(struct rune_module_stream * mStream, streamId_t id);


void runesTake(struct rune_stream * stream);

void runesRelease(struct rune_stream * stream);


size_t runesWrite(struct rune_stream * stream, const void * data, size_t sizeInBytes);
size_t runesRead(struct rune_stream * stream, void * data, size_t sizeInBytes);



void runemsMessageReceive(struct rune_module_stream * mStream,
                          struct rune_bit_buffer * buffer);
void runemsMessageFill(struct rune_module_stream * mStream,
                       uint8_t id,
                       struct rune_bit_buffer * buffer);
void runemsMessageNotification(struct rune_module_stream * mStream,
                               uint8_t id, 
                               enum rune_message_notification notification);




#endif // __RUNESTREAM_H__

