#ifndef __RUNEWRITABLE_H__
#define __RUNEWRITABLE_H__
#include <stdint.h>

struct rune_writable;

struct rune_writable_vt
{
   size_t (*fpWrite)(struct rune_writable * writable, 
                     size_t count, size_t elementSize, 
                     const void * data);
};

struct rune_writable
{
   const struct rune_writable_vt * vt;
};

static inline
size_t runewWrite(struct rune_writable * writable, 
                  size_t count, size_t elementSize,
                  const void * data)
{
   return writable->vt->fpWrite(writable, count, elementSize, data);
}

// Array implementation 


extern const struct rune_writable_vt WritableArrayVT;

struct runew_array
{
   struct rune_writable parent;
   uint8_t * array;
};

static inline
struct rune_writable * runewInit(struct runew_array * array, void * data)
{
   array->parent.vt = &WritableArrayVT;
   array->array = data;
   return &array->parent;
}

#endif // __RUNEWRITABLE_H__
