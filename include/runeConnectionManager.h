#ifndef __RUNECONNECTIONMANAGER_H__
#define __RUNECONNECTIONMANAGER_H__

#include <stddef.h>
#include "runeAddress.h"
#include "runeBitBuffer.h"

enum rune_connection_state
{
   eRUNECS_Connecting,
   eRUNECS_Connected,
   eRUNECS_Disconnecting,
   eRUNECE_Disconnected
};

enum rune_message_notification
{
   eRUNEMN_Dropped,
   eRUNEMN_Received
};

struct rune_connection_manager;
struct rune_connection;
struct rune_connection_settings;

typedef void (*NewConnection_FP)(struct rune_connection_manager * manager,
                                 struct rune_connection * connection,
                                 struct rune_connection_settings * settings);

typedef void (*SendMessage_FP)(struct rune_connection_manager * manager,
                               struct rune_connection * connection,
                               const void * buffer, size_t bufferSize,
                               struct rune_address * address);

typedef void (*MessageReceived_FP)(struct rune_connection * connection,
                                   struct rune_bit_buffer * buffer,
                                   void * userData);
typedef void (*MessageNotification_FP)(struct rune_connection * connection,
                                       uint8_t id, 
                                       enum rune_message_notification notification,
                                       void * userData);
typedef void (*MessageFill_FP)(struct rune_connection * connection,
                               uint8_t id,
                               struct rune_bit_buffer * buffer,
                               void * userData);

struct rune_connection_settings
{
   MessageReceived_FP messageReceivedCallback;
   MessageFill_FP messageFillCallback;
   MessageNotification_FP messageNotificationCallback;
   void * userData;
};

struct rune_connection_manager * runecmNew(void * userData,
                                           SendMessage_FP sendMessageCallback,
                                           NewConnection_FP newConnectionCallback);

void runecmTake(struct rune_connection_manager * manager);
void runecmRelease(struct rune_connection_manager * manager);

void runecmReceiveMessage(struct rune_connection_manager * manager, 
                          const void * buffer, size_t bufferSize, 
                          struct rune_address * address);


void runecmUpdate(struct rune_connection_manager * manager, int dt_ms);

struct rune_connection * runecmConnectTo(struct rune_connection_manager * manager, 
                                         struct rune_address * address);

void runecTake(struct rune_connection * connection);
void runecRelease(struct rune_connection * connection);

#endif // __RUNECONNECTIONMANAGER_H__

