#ifndef __RUNEADDRESSINT_H__
#define __RUNEADDRESSINT_H__

#include "runeAddress.h"

struct rune_address_int
{
   struct rune_address base;
   int address;
   char * text;
};

struct rune_address * runeAddressIntNew(int address);


#endif //__RUNEADDRESSINT_H__

