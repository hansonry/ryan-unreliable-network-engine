#ifndef __RUNEREADABLE_H__
#define __RUNEREADABLE_H__
#include <stdint.h>

struct rune_readable;

struct rune_readable_vt
{
   size_t (*fpRead)(struct rune_readable * readable, 
                    size_t count, size_t elementSize, 
                    void * data);
};

struct rune_readable
{
   const struct rune_readable_vt * vt;
};

static inline
size_t runerRead(struct rune_readable * readable, 
                 size_t count, size_t elementSize,
                 void * data)
{
   return readable->vt->fpRead(readable, count, elementSize, data);
}

// Array implementation 


extern const struct rune_readable_vt ReadableArrayVT;

struct runer_array
{
   struct rune_readable parent;
   const uint8_t * array;
};

static inline
struct rune_readable * runerInit(struct runer_array * array, const void * data)
{
   array->parent.vt = &ReadableArrayVT;
   array->array = data;
   return &array->parent;
}

#endif // __RUNEREADABLE_H__
