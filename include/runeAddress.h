#ifndef __RUNEADDRESS_H__
#define __RUNEADDRESS_H__


struct rune_address_vt;

struct rune_address
{
   const struct rune_address_vt * vt;
   unsigned int refCount;
};

struct rune_address_vt
{
   int (*fpCompare)(const struct rune_address * a, 
                    const struct rune_address * b);
   const char * (*fpGetText)(const struct rune_address * address);
   void (*fpFree)(struct rune_address * address);
};


static inline
int runeAddressCompare(const struct rune_address * a, 
                       const struct rune_address * b)
{
   if(a == b)
   {
      return 0;
   }
   if(a->vt != b->vt ||
      a->vt->fpCompare == NULL)
   {
      return 255;
   }
   return a->vt->fpCompare(a, b);
}

static inline
const char * runeAddressGetText(const struct rune_address * address)
{
   if(address->vt->fpGetText == NULL)
   {
      return "?Unknwonw?";
   }
   return address->vt->fpGetText(address);
}

static inline
void runeAddressFree(struct rune_address * address)
{
   address->vt->fpFree(address);
}

static inline
struct rune_address * runeAddressInit(struct rune_address * address, 
                                      const struct rune_address_vt * vt)
{
   address->vt       = vt;
   address->refCount = 1;
   return address;
}

static inline
void runeAddressTake(struct rune_address * address)
{
   address->refCount ++;
}

static inline
void runeAddressRelease(struct rune_address * address)
{
   if(address->refCount > 1)
   {
      address->refCount --;
   }
   else
   {
      address->refCount = 0;
      runeAddressFree(address);
   }
}

#endif // __RUNEADDRESS_H__


