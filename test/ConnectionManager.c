#include <stdint.h>
#include <ryanmock.h>
#include <runeAddressInt.h>
#include <runeConnectionManager.h>

RMM_SYMBOLTABLE_LOAD(ConnectionManager)

static 
void (*runecBuildAndSendMessage)(struct rune_connection * connection);


static 
void setup(void)
{
   rmmSymbolTableLink(ConnectionManager, runecBuildAndSendMessage);
}

static
void MockedNewConnectionCallback(struct rune_connection_manager * manager,
                                 struct rune_connection * connection,
                                 struct rune_connection_settings * settings)
{
   rmmFunctionCalled();
   rmmParamCheck(manager);
   rmmParamCheck(connection);
   rmmParamCheck(settings);
}

static
void MockedSendMessageCallback(struct rune_connection_manager * manager,
                               struct rune_connection * connection,
                               const void * buffer, size_t bufferSize,
                               struct rune_address * address)
{
   rmmFunctionCalled();
   rmmParamCheck(manager);
   rmmParamCheck(connection);
   rmmParamCheck(bufferSize);
   rmmParamCheckVoidArray(buffer, bufferSize);
   rmmParamCheck(address);
}

void MockedMessageReceivedCallback(struct rune_connection * connection,
                                   struct rune_bit_buffer * buffer,
                                   void * userData)
{
   rmmFunctionCalled();
   rmmParamCheck(connection);
   rmmParamCheck(buffer);
   rmmParamCheck(userData);
}

void MockedMessageNotificationCallback(struct rune_connection * connection,
                                       uint8_t id, 
                                       enum rune_message_notification notification,
                                       void * userData)
{
   rmmFunctionCalled();
   rmmParamCheck(connection);
   rmmParamCheck(id);
   rmmParamCheck(notification);
   rmmParamCheck(userData);
}

void MockedMessageFillCallback(struct rune_connection * connection,
                               uint8_t id,
                               struct rune_bit_buffer * buffer,
                               void * userData)
{
   rmmFunctionCalled();
   rmmParamCheck(connection);
   rmmParamCheck(id);
   rmmParamCheck(buffer);
   rmmParamCheck(userData);
}

static int ConnectionData = 5;
static void * ConnectionUserData = &ConnectionData;

static
void NewConnectionCallback(struct rune_connection_manager * manager,
                           struct rune_connection * connection,
                           struct rune_connection_settings * settings)
{
   rmmAssertPtrNotNULL(manager);
   rmmAssertPtrNotNULL(connection);
   settings->messageReceivedCallback     = MockedMessageReceivedCallback;
   settings->messageFillCallback         = MockedMessageFillCallback;
   settings->messageNotificationCallback = MockedMessageNotificationCallback;
   settings->userData = ConnectionUserData;
}



#define BUFFER_SIZE 512

struct buffer
{
   size_t bufferSize;
   struct rune_bit_buffer bitBuffer;
   uint8_t base[BUFFER_SIZE];
};

static inline
void buffer_init(struct buffer * buffer)
{
   runebbInitWriter(&buffer->bitBuffer, buffer->base, BUFFER_SIZE);
}

static inline
void buffer_finalize(struct buffer * buffer)
{
   runebbWriteRemainingBits(&buffer->bitBuffer, false);
   buffer->bufferSize = runebbGetBytesWritten(&buffer->bitBuffer);
}

struct manager_data
{
   struct buffer * tx;
   struct buffer * rx;
   struct rune_connection_manager * manager;
};

static inline
void manager_data_init(struct manager_data * managerData, 
                       void * userData, 
                       SendMessage_FP sendMessageCallback,
                       NewConnection_FP newConnectionCallback,
                       struct buffer * tx, 
                       struct buffer * rx)
{
   managerData->tx = tx;
   managerData->rx = rx;
   managerData->manager = runecmNew(userData, 
                                    sendMessageCallback, 
                                    newConnectionCallback);
}

static inline
void manager_data_init_all(struct manager_data * managerData, 
                           void * userData, 
                           SendMessage_FP sendMessageCallback,
                           NewConnection_FP newConnectionCallback,
                           struct buffer * tx, 
                           struct buffer * rx)
{
   buffer_init(tx);
   buffer_init(rx);
   manager_data_init(managerData, userData, 
                     sendMessageCallback, newConnectionCallback,
                     tx, rx);
}

static inline
void manager_data_cleanup(struct manager_data * testData)
{
   runecmRelease(testData->manager);
}

#define util_expect_MockedSendMessageCallback(manager, connection, address, buffer) \
rmmEnterFunction(util_expect_MockedSendMessageCallback);                            \
_util_expect_MockedSendMessageCallback(manager, connection, address, buffer);       \
rmmExitFunction(util_expect_MockedSendMessageCallback)

static inline
void _util_expect_MockedSendMessageCallback(struct rune_connection_manager * manager,
                                            struct rune_connection * connection,
                                            struct rune_address * address,
                                            struct buffer * buffer)
{
   rmmMockBeginOrdered(MockedSendMessageCallback);
      rmmExpectPtrEqual(manager, manager);
      if(connection == NULL)
      {
         rmmExpectPtrNotNULL(connection);
      }
      else
      {
         rmmExpectPtrEqual(connection, connection);
      }
      rmmExpectPtrEqual(address, address);
      rmmExpectSizeTEqual(bufferSize, buffer->bufferSize);
      rmmExpectMemoryEqual(buffer, buffer->base, buffer->bufferSize);
   rmmMockEnd();
}

#define util_expect_MockedMessageNotificationCallback(connection, messageId, notification) \
rmmEnterFunction(util_expect_MockedMessageNotificationCallback);                           \
_util_expect_MockedMessageNotificationCallback(connection, messageId, notification);       \
rmmExitFunction(util_expect_MockedMessageNotificationCallback)

static inline
void _util_expect_MockedMessageNotificationCallback(struct rune_connection * connection,
                                                    uint8_t messageId,
                                                    enum rune_message_notification notification)
{
   rmmMockBeginOrdered(MockedMessageNotificationCallback);
      rmmExpectPtrEqual(connection, connection);
      rmmExpectIntU8Equal(id, messageId);
      rmmExpectEnumEqual(notification, notification);
      rmmExpectPtrEqual(userData, ConnectionUserData);
   rmmMockEnd();
}

void test_NewOutgoingConnection(void)
{
   struct manager_data data;
   struct buffer rx, tx;
   struct rune_address * address;
   struct rune_connection * connection;
   
   manager_data_init_all(&data, NULL, 
                         MockedSendMessageCallback, NewConnectionCallback, 
                         &tx, &rx);
                         
   address = runeAddressIntNew(5);

   // Fill Settings
 
   rmmMockBeginOrdered(MockedMessageFillCallback);
      rmmExpectPtrNotNULL(connection);
      rmmExpectIntU8Equal(id, 0);
      rmmExpectPtrNotNULL(buffer);
      rmmExpectPtrEqual(userData, ConnectionUserData);
   rmmMockEnd();
   
   // Send out first message
   runebbReset(&tx.bitBuffer);
   runebbWriteByte(&tx.bitBuffer, 0);
   runebbWriteBit(&tx.bitBuffer, false);
   buffer_finalize(&tx);
   
   util_expect_MockedSendMessageCallback(data.manager, NULL, address, &tx);
 
   connection = runecmConnectTo(data.manager, address);
   
   
   
   // Send first response
   runebbReset(&rx.bitBuffer);
   runebbWriteByte(&rx.bitBuffer, 0);
   runebbWriteBit(&rx.bitBuffer, true);
   runebbWriteByte(&rx.bitBuffer, 0);
   runebbWriteBit(&rx.bitBuffer, false);
   buffer_finalize(&rx);
   
   
   util_expect_MockedMessageNotificationCallback(connection, 0, 
                                                 eRUNEMN_Received);
   
   rmmMockBeginOrdered(MockedMessageReceivedCallback);
      rmmExpectPtrEqual(connection, connection);
      rmmExpectPtrNotNULL(buffer);
      rmmExpectPtrEqual(userData, ConnectionUserData);
   rmmMockEnd();
   
   runecmReceiveMessage(data.manager, rx.base, rx.bufferSize, address); 
 
   runeAddressRelease(address);
   manager_data_cleanup(&data);
}

void test_NewIncomingConnection(void)
{
   size_t bufferSize = 4;
   uint8_t buffer[] = {1, 2, 3, 4};
   struct rune_connection_manager * manager = runecmNew(NULL,
                                                        MockedSendMessageCallback, 
                                                        MockedNewConnectionCallback);
   struct rune_address * address = runeAddressIntNew(5);

 
   rmmMockBeginOrdered(MockedNewConnectionCallback);
      rmmExpectPtrEqual(manager, manager);
      rmmExpectPtrNotNULL(connection);
      rmmExpectPtrNotNULL(settings);
   rmmMockEnd();
 
   runecmReceiveMessage(manager, buffer, bufferSize, address); 
   runeAddressRelease(address);

   runecmRelease(manager);
}

void test_MissingMessages(void)
{
   struct manager_data data;
   struct buffer rx, tx;
   struct rune_address * address;
   struct rune_connection * connection;
   
   manager_data_init_all(&data, NULL, 
                         MockedSendMessageCallback, NewConnectionCallback, 
                         &tx, &rx);
                         
   address = runeAddressIntNew(5);

   // Fill Settings
   rmmMockBeginOrdered(MockedMessageFillCallback);
      rmmExpectPtrNotNULL(connection);
      rmmExpectIntU8Equal(id, 0);
      rmmExpectPtrNotNULL(buffer);
      rmmExpectPtrEqual(userData, ConnectionUserData);
   rmmMockEnd();
   
   // Send out first message
   runebbReset(&tx.bitBuffer);
   runebbWriteByte(&tx.bitBuffer, 0);
   runebbWriteBit(&tx.bitBuffer, false);
   buffer_finalize(&tx);
   
   util_expect_MockedSendMessageCallback(data.manager, NULL, address, &tx);
 
   connection = runecmConnectTo(data.manager, address);
   
   
   
   // Send first response
   runebbReset(&rx.bitBuffer);
   runebbWriteByte(&rx.bitBuffer, 0);
   runebbWriteBit(&rx.bitBuffer, true);
   runebbWriteByte(&rx.bitBuffer, 0);
   runebbWriteBit(&rx.bitBuffer, false);
   buffer_finalize(&rx);
   
   
   util_expect_MockedMessageNotificationCallback(connection, 0, 
                                                 eRUNEMN_Received);
   
   rmmMockBeginOrdered(MockedMessageReceivedCallback);
      rmmExpectPtrEqual(connection, connection);
      rmmExpectPtrNotNULL(buffer);
      rmmExpectPtrEqual(userData, ConnectionUserData);
   rmmMockEnd();
   
   runecmReceiveMessage(data.manager, rx.base, rx.bufferSize, address); 
 
   // Send out Second message
   // Fill Settings
   rmmMockBeginOrdered(MockedMessageFillCallback);
      rmmExpectPtrNotNULL(connection);
      rmmExpectIntU8Equal(id, 1);
      rmmExpectPtrNotNULL(buffer);
      rmmExpectPtrEqual(userData, ConnectionUserData);
   rmmMockEnd();
   
   runebbReset(&tx.bitBuffer);
   runebbWriteByte(&tx.bitBuffer, 1);
   runebbWriteBit(&tx.bitBuffer, true);
   runebbWriteByte(&tx.bitBuffer, 0);
   runebbWriteBit(&tx.bitBuffer, false);
   buffer_finalize(&tx);
   
   util_expect_MockedSendMessageCallback(data.manager, connection, address, &tx);

   runecBuildAndSendMessage(connection);

   // Pretend message was dropped
   
   // Send out Third message
   // Fill Settings
   rmmMockBeginOrdered(MockedMessageFillCallback);
      rmmExpectPtrNotNULL(connection);
      rmmExpectIntU8Equal(id, 2);
      rmmExpectPtrNotNULL(buffer);
      rmmExpectPtrEqual(userData, ConnectionUserData);
   rmmMockEnd();
   
   runebbReset(&tx.bitBuffer);
   runebbWriteByte(&tx.bitBuffer, 2);
   runebbWriteBit(&tx.bitBuffer, false);
   buffer_finalize(&tx);
   
   util_expect_MockedSendMessageCallback(data.manager, connection, address, &tx);

   runecBuildAndSendMessage(connection);
   
   // Send second response
   runebbReset(&rx.bitBuffer);
   runebbWriteByte(&rx.bitBuffer, 1);
   runebbWriteBit(&rx.bitBuffer, true);
   runebbWriteByte(&rx.bitBuffer, 2);
   runebbWriteBit(&rx.bitBuffer, false);
   buffer_finalize(&rx);
   
   util_expect_MockedMessageNotificationCallback(connection, 1, 
                                                 eRUNEMN_Dropped); 
   util_expect_MockedMessageNotificationCallback(connection, 2, 
                                                 eRUNEMN_Received);

   
   rmmMockBeginOrdered(MockedMessageReceivedCallback);
      rmmExpectPtrEqual(connection, connection);
      rmmExpectPtrNotNULL(buffer);
      rmmExpectPtrEqual(userData, ConnectionUserData);
   rmmMockEnd();
   
   runecmReceiveMessage(data.manager, rx.base, rx.bufferSize, address); 

   runeAddressRelease(address);
   manager_data_cleanup(&data);
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_NewIncomingConnection),
      rmmMakeTest(test_NewOutgoingConnection),
      rmmMakeTest(test_MissingMessages),
   };
   return rmmRunTestsCmdLine2(tests, "ConnectionManager", setup, NULL, argc, args);
}


