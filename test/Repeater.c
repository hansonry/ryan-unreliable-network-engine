#include <stdint.h>
#include <ryanmock.h>
#include <runeRepeater.h>


struct test_repeater
{
   struct rune_repeater parent;
};

static 
void testFillMessage(struct rune_repeater * repeater,
                     uint8_t id, 
                     struct rune_bit_buffer * buffer)
{
   rmmFunctionCalled();
   rmmParamCheck(repeater);
   rmmParamCheck(id);
   rmmParamCheck(buffer);
}

static
void testNotification(struct rune_repeater * repeater, 
                      uint8_t id,
                      enum rune_message_notification notification)
{
   rmmFunctionCalled();
   rmmParamCheck(repeater);
   rmmParamCheck(id);
   rmmParamCheck(notification);
}


const static struct rune_repeater_vt test_vt = {
   testFillMessage,
   testNotification
};

static inline
struct rune_repeater * test_repeater_init(struct test_repeater * repeater,
                                          int retries)
{
   return runerepInit(&repeater->parent, &test_vt, retries);
}

void test_Initialization(void)
{
   struct test_repeater test_repeater;
   struct rune_repeater * repeater;

   repeater = test_repeater_init(&test_repeater, 3);

   rmmAssertPtrEqual(&test_repeater.parent, repeater);
   rmmAssertPtrEqual(repeater->vt, &test_vt);
   rmmAssertIntEqual(repeater->retryMax, 3);
   rmmAssertIntEqual(repeater->attempts, 0);
   rmmAssertEnumEqual(repeater->state, eRUNEREP_WaitingToSend);
   rmmAssertIntU8Equal(repeater->id, 0);
}

void test_Send(void)
{
   struct test_repeater test_repeater;
   struct rune_repeater * repeater;
   struct rune_bit_buffer buffer;

   repeater = test_repeater_init(&test_repeater, 3);

   repeater->state = eRUNEREP_WaitingToSend;
   repeater->attempts = 0;
   repeater->id = 0;

   rmmMockBeginOrdered(testFillMessage);
      rmmExpectPtrEqual(repeater, repeater);
      rmmExpectIntU8Equal(id, 1);
      rmmExpectPtrEqual(buffer, &buffer);
   rmmMockEnd();

   // FUT
   runemrepMessageFill(repeater, 1, &buffer); 
   
   rmmAssertIntEqual(repeater->attempts, 1);
   rmmAssertEnumEqual(repeater->state, eRUNEREP_WaitingForResponse);
   rmmAssertIntU8Equal(repeater->id, 1);
   
   repeater->state = eRUNEREP_WaitingToSend;
   repeater->attempts = 1;
   repeater->id = 0;

   rmmMockBeginOrdered(testFillMessage);
      rmmExpectPtrEqual(repeater, repeater);
      rmmExpectIntU8Equal(id, 4);
      rmmExpectPtrEqual(buffer, &buffer);
   rmmMockEnd();

   // FUT
   runemrepMessageFill(repeater, 4, &buffer); 
   
   rmmAssertIntEqual(repeater->attempts, 2);
   rmmAssertEnumEqual(repeater->state, eRUNEREP_WaitingForResponse);
   rmmAssertIntU8Equal(repeater->id, 4);
}


void test_SendSkip(void)
{
   struct test_repeater test_repeater;
   struct rune_repeater * repeater;
   struct rune_bit_buffer buffer;

   repeater = test_repeater_init(&test_repeater, 3);

   repeater->state = eRUNEREP_WaitingForResponse;
   repeater->attempts = 0;
   repeater->id = 0;

   // FUT
   runemrepMessageFill(repeater, 2, &buffer); 
   
   rmmAssertIntEqual(repeater->attempts, 0);
   rmmAssertEnumEqual(repeater->state, eRUNEREP_WaitingForResponse);
   rmmAssertIntU8Equal(repeater->id, 0);
}

void test_NotifyComplete(void)
{
   struct test_repeater test_repeater;
   struct rune_repeater * repeater;

   repeater = test_repeater_init(&test_repeater, 3);

   repeater->state = eRUNEREP_WaitingForResponse;
   repeater->attempts = 1;
   repeater->id = 2;
   
   rmmMockBeginOrdered(testNotification);
      rmmExpectPtrEqual(repeater, repeater);
      rmmExpectIntU8Equal(id, 2);
      rmmExpectEnumEqual(notification, eRUNEMN_Received);
   rmmMockEnd();

   // FUT
   runemrepMessageNotification(repeater, 2, eRUNEMN_Received); 
   
   rmmAssertIntEqual(repeater->attempts, 1);
   rmmAssertEnumEqual(repeater->state, eRUNEREP_Complete);
   rmmAssertIntU8Equal(repeater->id, 0);
}

void test_NotifyFailure(void)
{
   struct test_repeater test_repeater;
   struct rune_repeater * repeater;

   repeater = test_repeater_init(&test_repeater, 3);

   repeater->state = eRUNEREP_WaitingForResponse;
   repeater->attempts = 3;
   repeater->id = 2;
   
   rmmMockBeginOrdered(testNotification);
      rmmExpectPtrEqual(repeater, repeater);
      rmmExpectIntU8Equal(id, 2);
      rmmExpectEnumEqual(notification, eRUNEMN_Dropped);
   rmmMockEnd();

   // FUT
   runemrepMessageNotification(repeater, 2, eRUNEMN_Dropped); 
   
   rmmAssertIntEqual(repeater->attempts, 3);
   rmmAssertEnumEqual(repeater->state, eRUNEREP_Failure);
   rmmAssertIntU8Equal(repeater->id, 0);
}

void test_NotifySkipState(void)
{
   struct test_repeater test_repeater;
   struct rune_repeater * repeater;

   repeater = test_repeater_init(&test_repeater, 3);

   repeater->state = eRUNEREP_WaitingToSend;
   repeater->attempts = 1;
   repeater->id = 2;
   
   // FUT
   runemrepMessageNotification(repeater, 2, eRUNEMN_Received); 
   
   rmmAssertIntEqual(repeater->attempts, 1);
   rmmAssertEnumEqual(repeater->state, eRUNEREP_WaitingToSend);
   rmmAssertIntU8Equal(repeater->id, 2);
}

void test_NotifySkipMessageId(void)
{
   struct test_repeater test_repeater;
   struct rune_repeater * repeater;

   repeater = test_repeater_init(&test_repeater, 3);

   repeater->state = eRUNEREP_WaitingToSend;
   repeater->attempts = 1;
   repeater->id = 2;
   
   // FUT
   runemrepMessageNotification(repeater, 3, eRUNEMN_Received); 
   
   rmmAssertIntEqual(repeater->attempts, 1);
   rmmAssertEnumEqual(repeater->state, eRUNEREP_WaitingToSend);
   rmmAssertIntU8Equal(repeater->id, 2);
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_Initialization),
      rmmMakeTest(test_Send),
      rmmMakeTest(test_SendSkip),
      rmmMakeTest(test_NotifyComplete),
      rmmMakeTest(test_NotifyFailure),
      rmmMakeTest(test_NotifySkipState),
      rmmMakeTest(test_NotifySkipMessageId),
   };
   return rmmRunTestsCmdLine(tests, "Repeater", argc, args);
}


