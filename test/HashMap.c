#include <ryanmock.h>
#include <runeHashMap.h>

RUNE_HASHMAP_TYPE_CREATE(int, int, int)

void test_AddAndRemove(void)
{
   struct rune_hashmap_int map;
   runehmInit_int(&map, 0);
   rmmAssertSizeTEqual(0, runehmCount_int(&map));
   
   runehmPut_int(&map, 1, 5);
   rmmAssertSizeTEqual(1, runehmCount_int(&map));
   rmmAssertSizeTEqual(5, runehmGet_int(&map, 1));
   
   rmmAssertTrue(runehmRemove_int(&map, 1));
   rmmAssertSizeTEqual(0, runehmCount_int(&map));
   
   runehmCleanup_int(&map);
}

void test_AddAndRemoveToSameBucket(void)
{
   struct rune_hashmap_int map;
   runehmInit_int(&map, 1);
   rmmAssertSizeTEqual(0, runehmCount_int(&map));
   
   rmmAssertFalse(runehmRemove_int(&map, 1));
   
   runehmPut_int(&map, 1, 5);
   runehmPut_int(&map, 2, 4);
   rmmAssertSizeTEqual(2, runehmCount_int(&map));
   rmmAssertTrue(runehmHasKey_int(&map, 1));
   rmmAssertTrue(runehmHasKey_int(&map, 2));
   rmmAssertSizeTEqual(5, runehmGet_int(&map, 1));
   rmmAssertSizeTEqual(4, runehmGet_int(&map, 2));
   
   
   rmmAssertTrue(runehmRemove_int(&map, 1));
   rmmAssertFalse(runehmRemove_int(&map, 1));
   rmmAssertFalse(runehmHasKey_int(&map, 1));
   rmmAssertTrue(runehmHasKey_int(&map, 2));
   rmmAssertSizeTEqual(1, runehmCount_int(&map));
   rmmAssertSizeTEqual(4, runehmGet_int(&map, 2));

   rmmAssertTrue(runehmRemove_int(&map, 2));
   rmmAssertFalse(runehmRemove_int(&map, 2));
   rmmAssertFalse(runehmHasKey_int(&map, 2));
   rmmAssertSizeTEqual(0, runehmCount_int(&map));
   
   runehmCleanup_int(&map);
}

void test_AddAndRemoveToOtherBuckets(void)
{
   struct rune_hashmap_int map;
   runehmInit_int(&map, 256);
   rmmAssertSizeTEqual(0, runehmCount_int(&map));
   
   rmmAssertFalse(runehmRemove_int(&map, 1));
   
   runehmPut_int(&map, 1, 5);
   runehmPut_int(&map, 2, 4);
   rmmAssertSizeTEqual(2, runehmCount_int(&map));
   rmmAssertTrue(runehmHasKey_int(&map, 1));
   rmmAssertTrue(runehmHasKey_int(&map, 2));
   rmmAssertSizeTEqual(5, runehmGet_int(&map, 1));
   rmmAssertSizeTEqual(4, runehmGet_int(&map, 2));
   
   
   rmmAssertTrue(runehmRemove_int(&map, 1));
   rmmAssertFalse(runehmRemove_int(&map, 1));
   rmmAssertFalse(runehmHasKey_int(&map, 1));
   rmmAssertTrue(runehmHasKey_int(&map, 2));
   rmmAssertSizeTEqual(1, runehmCount_int(&map));
   rmmAssertSizeTEqual(4, runehmGet_int(&map, 2));

   rmmAssertTrue(runehmRemove_int(&map, 2));
   rmmAssertFalse(runehmRemove_int(&map, 2));
   rmmAssertFalse(runehmHasKey_int(&map, 2));
   rmmAssertSizeTEqual(0, runehmCount_int(&map));
   
   runehmCleanup_int(&map);
}

void test_Clear(void)
{
   struct rune_hashmap_int map;
   runehmInit_int(&map, 0);
   rmmAssertSizeTEqual(0, runehmCount_int(&map));
   
   rmmAssertFalse(runehmRemove_int(&map, 1));
   
   runehmPut_int(&map, 1, 5);
   runehmPut_int(&map, 2, 4);
   rmmAssertSizeTEqual(2, runehmCount_int(&map));

   runehmClear_int(&map);
   rmmAssertSizeTEqual(0, runehmCount_int(&map));
   
   runehmCleanup_int(&map);
}


int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_AddAndRemove),
      rmmMakeTest(test_AddAndRemoveToSameBucket),
      rmmMakeTest(test_AddAndRemoveToOtherBuckets),
      rmmMakeTest(test_Clear),
   };
   return rmmRunTestsCmdLine2(tests, "HashMap", NULL, NULL, argc, args);
}