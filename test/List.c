#include <stdint.h>
#include <ryanmock.h>
#include <runeList.h>

RUNE_LIST_TYPE_CREATE(int, int)

#define DEFAULT_GROWBY 32


static inline
void * util_get_offset(void * base, size_t elementSize, size_t index)
{
   uint8_t * u8Base = base;
   return &u8Base[elementSize * index];
}

static inline 
void util_add_ints(struct rune_list_int * list, const int * ints, size_t count)
{
   size_t i;
   for(i = 0; i < count; i++)
   {
      (void)runelAddCopy_int(list, ints[i], NULL);
   }
}

void test_InitDefaultsAndCleanup(void)
{
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, 0);

   rmmAssertSizeTEqual(list.list.count,  0);
   rmmAssertSizeTEqual(list.list.growBy, DEFAULT_GROWBY);
   rmmAssertSizeTEqual(list.list.size,   DEFAULT_GROWBY);
   rmmAssertSizeTEqual(list.list.elementSize, sizeof(int));
   rmmAssertPtrNotNULL(list.list.base);

   runelCleanup_int(&list);
   
   rmmAssertPtrNULL(list.list.base);
   rmmAssertSizeTEqual(list.list.count, 0);
   rmmAssertSizeTEqual(list.list.size, 0);
   rmmAssertSizeTEqual(list.list.elementSize, 0);
   rmmAssertSizeTEqual(list.list.growBy, 0);

}

void test_InitSized(void)
{
   const size_t testSize = 2;
   struct rune_list_int list = {0};

   runelInit_int(&list, testSize, 0);

   rmmAssertSizeTEqual(list.list.count,  0);
   rmmAssertSizeTEqual(list.list.growBy, DEFAULT_GROWBY);
   rmmAssertSizeTEqual(list.list.size,   testSize);
   rmmAssertSizeTEqual(list.list.elementSize, sizeof(int));
   rmmAssertPtrNotNULL(list.list.base);

   runelCleanup_int(&list);
   
}

void test_InitGrowBy(void)
{
   const size_t testGrowBy = 64;
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, testGrowBy);

   rmmAssertSizeTEqual(list.list.count,  0);
   rmmAssertSizeTEqual(list.list.growBy, testGrowBy);
   rmmAssertSizeTEqual(list.list.size,   testGrowBy);
   rmmAssertSizeTEqual(list.list.elementSize, sizeof(int));
   rmmAssertPtrNotNULL(list.list.base);

   runelCleanup_int(&list);
   
}

void test_Add(void)
{
   int * item;
   size_t index;
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, 0);

   // Add First Item
   item = runelAdd_int(&list, &index);

   rmmAssertSizeTEqual(list.list.count, 1);
   rmmAssertSizeTEqual(list.list.size,  DEFAULT_GROWBY);
   rmmAssertSizeTEqual(index,           0);
   rmmAssertPtrEqual(item,              util_get_offset(list.list.base, sizeof(int), index));
   
   // Add Second Item
   item = runelAdd_int(&list, &index);

   rmmAssertSizeTEqual(list.list.count, 2);
   rmmAssertSizeTEqual(list.list.size,  DEFAULT_GROWBY);
   rmmAssertSizeTEqual(index,           1);
   rmmAssertPtrEqual(item,              util_get_offset(list.list.base, sizeof(int), index));
   
   // Add Third Item
   item = runelAdd_int(&list, NULL);

   rmmAssertSizeTEqual(list.list.count, 3);
   rmmAssertSizeTEqual(list.list.size,  DEFAULT_GROWBY);
   rmmAssertPtrEqual(item,              util_get_offset(list.list.base, sizeof(int), 2));

   runelCleanup_int(&list);   
}

void test_AddGrow(void)
{
   void * item;
   size_t index;
   const size_t growBy = 4;
   const int fillData[] = { 1, 2, 3, 4 };
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, growBy);

   util_add_ints(&list, fillData, growBy);

   
   rmmAssertSizeTEqual(list.list.count, growBy);
   rmmAssertSizeTEqual(list.list.size,  growBy);
   
   item = runelAdd_int(&list, &index);

   rmmAssertSizeTEqual(list.list.count, growBy + 1);
   rmmAssertSizeTEqual(list.list.size,  2 * growBy + 1);
   rmmAssertSizeTEqual(index,           growBy);
   rmmAssertPtrEqual(item,              util_get_offset(list.list.base, sizeof(int), index));

   runelCleanup_int(&list);
}

void test_AddCopy(void)
{
   void * item;
   size_t index;
   int value;
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, 0);

   // Add First Item
   value = 1;
   item = runelAddCopy_int(&list, value, &index);

   rmmAssertSizeTEqual(list.list.count, 1);
   rmmAssertSizeTEqual(list.list.size,  DEFAULT_GROWBY);
   rmmAssertSizeTEqual(index,           0);
   rmmAssertPtrEqual(item,              util_get_offset(list.list.base, sizeof(int), index));
   rmmAssertMemoryEqual(item,           &value, 1);
   
   // Add Second Item
   value = 2;
   item = runelAddCopy_int(&list, value, &index);

   rmmAssertSizeTEqual(list.list.count, 2);
   rmmAssertSizeTEqual(list.list.size,  DEFAULT_GROWBY);
   rmmAssertSizeTEqual(index,           1);
   rmmAssertPtrEqual(item,              util_get_offset(list.list.base, sizeof(int), index));
   rmmAssertMemoryEqual(item,           &value, 1);
   
   // Add Third Item
   value = 2;
   item = runelAddCopy_int(&list, value, NULL);

   rmmAssertSizeTEqual(list.list.count, 3);
   rmmAssertSizeTEqual(list.list.size,  DEFAULT_GROWBY);
   rmmAssertPtrEqual(item,              util_get_offset(list.list.base, sizeof(int), 2));
   rmmAssertMemoryEqual(item,           &value, 1);

   runelCleanup_int(&list);   
}

void test_AddCopyMany(void)
{
   void * item;
   size_t index;
   const size_t listSize = 5;
   const int values[] = {1, 2, 3, 4, 5};
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, 0);

   item = runelAddCopyMany_int(&list, values, listSize, &index);

   rmmAssertSizeTEqual(list.list.count, listSize);
   rmmAssertSizeTEqual(index,           0);
   rmmAssertPtrEqual(item,              util_get_offset(list.list.base, sizeof(int), index));
   rmmAssertMemoryEqual(item,           values, listSize);
   

   runelCleanup_int(&list);   
}


void test_Clear(void)
{
   const size_t fillSize = 4;
   const int fillData[] = { 1, 2, 3, 4 };
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, 0);

   util_add_ints(&list, fillData, fillSize);

   rmmAssertSizeTEqual(list.list.count, fillSize);
   
   runelClear_int(&list);
   
   rmmAssertSizeTEqual(list.list.count, 0);

   runelCleanup_int(&list);   
}

void test_RemoveFast(void)
{
   const size_t fillSize = 4;
   const int fillData[] = { 1, 2, 3, 4 };
   const int expectedData[] = { 3, 2 };
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, 0);

   util_add_ints(&list, fillData, fillSize);

   rmmAssertSizeTEqual(list.list.count, fillSize);
  
   // Try to remove after the last element 
   rmmAssertFalse(runelRemoveFast_int(&list, fillSize));  

   rmmAssertSizeTEqual(list.list.count, fillSize);
   rmmAssertMemoryEqual(list.list.base, fillData, fillSize);
   
   // Remove Last Element
   rmmAssertTrue(runelRemoveFast_int(&list, fillSize - 1));  

   rmmAssertSizeTEqual(list.list.count, fillSize - 1);
   rmmAssertMemoryEqual(list.list.base, fillData, fillSize - 1);
   
   // Remove First Element
   rmmAssertTrue(runelRemoveFast_int(&list, 0));  

   rmmAssertSizeTEqual(list.list.count, fillSize - 2);
   rmmAssertMemoryEqual(list.list.base, expectedData, fillSize - 2);

   runelCleanup_int(&list);   
}

void test_RemoveOrdered(void)
{
   const size_t fillSize = 4;
   const int fillData[] = { 1, 2, 3, 4 };
   const int expectedData[] = { 2, 3 };
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, 0);

   util_add_ints(&list, fillData, fillSize);

   rmmAssertSizeTEqual(list.list.count, fillSize);
  
   // Try to remove after the last element 
   rmmAssertFalse(runelRemoveOrdered_int(&list, fillSize));  

   rmmAssertSizeTEqual(list.list.count, fillSize);
   rmmAssertMemoryEqual(list.list.base, fillData, fillSize);
   
   // Remove Last Element
   rmmAssertTrue(runelRemoveOrdered_int(&list, fillSize - 1));  

   rmmAssertSizeTEqual(list.list.count, fillSize - 1);
   rmmAssertMemoryEqual(list.list.base, fillData, fillSize - 1);
   
   // Remove First Element
   rmmAssertTrue(runelRemoveOrdered_int(&list, 0));  

   rmmAssertSizeTEqual(list.list.count, fillSize - 2);
   rmmAssertMemoryEqual(list.list.base, expectedData, fillSize - 2);

   runelCleanup_int(&list);   
}

void test_GetCount(void)
{
   const size_t fillSize = 4;
   const int fillData[] = { 1, 2, 3, 4 };
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, 0);

   util_add_ints(&list, fillData, fillSize);

   rmmAssertSizeTEqual(runelGetCount_int(&list), fillSize);
   
   runelCleanup_int(&list);   
}

void test_GetPtrAndValue(void)
{
   const size_t fillSize = 4;
   const int fillData[] = { 1, 2, 3, 4 };
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, 0);

   util_add_ints(&list, fillData, fillSize);

   rmmAssertPtrEqual(runelGetPtr_int(&list, 1), util_get_offset(list.list.base, list.list.elementSize, 1));
   rmmAssertIntEqual(runelGetValue_int(&list, 1), fillData[1]);
   
   runelCleanup_int(&list);   
}

void test_GetAll(void)
{
   const size_t fillSize = 4;
   size_t count;
   const int fillData[] = { 1, 2, 3, 4 };
   struct rune_list_int list = {0};

   runelInit_int(&list, 0, 0);

   util_add_ints(&list, fillData, fillSize);

   rmmAssertPtrEqual(runelGetAll_int(&list, &count), list.list.base);
   rmmAssertSizeTEqual(count, fillSize);
   rmmAssertPtrEqual(runelGetAll_int(&list, NULL), list.list.base);
   
   runelCleanup_int(&list);   
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_InitDefaultsAndCleanup),
      rmmMakeTest(test_InitSized),
      rmmMakeTest(test_InitGrowBy),
      rmmMakeTest(test_Add),
      rmmMakeTest(test_AddGrow),
      rmmMakeTest(test_AddCopy),
      rmmMakeTest(test_AddCopyMany),
      rmmMakeTest(test_Clear),
      rmmMakeTest(test_RemoveFast),
      rmmMakeTest(test_RemoveOrdered),
      rmmMakeTest(test_GetCount),
      rmmMakeTest(test_GetPtrAndValue),
      rmmMakeTest(test_GetAll),
   };
   return rmmRunTestsCmdLine(tests, "List", argc, args);
}


