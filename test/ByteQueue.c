#include <ryanmock.h>
#include <runeByteQueue.h>


void test_basicReadAndWrite(void)
{
   struct rune_bytequeue queue;
   const size_t dataSize = 5;
   const uint8_t writeData[] = {1, 2, 3, 4, 5};
   uint8_t readData[5] = {0, 0, 0, 0, 0};
   
   runebqInit(&queue, 0, 0);
   
   rmmAssertSizeTEqual(runebqCount(&queue), 0);
   
   // Add some stuff in bits
   rmmAssertSizeTEqual(runebqWrite(&queue, writeData, 1), 1);
   rmmAssertSizeTEqual(runebqCount(&queue), 1);
   
   rmmAssertSizeTEqual(runebqWrite(&queue, writeData + 1, dataSize - 1), dataSize - 1);
   rmmAssertSizeTEqual(runebqCount(&queue), dataSize);
   
   // Remove some stuff in bits
   
   rmmAssertSizeTEqual(runebqRead(&queue, readData, 3), 3);
   rmmAssertMemoryEqual(readData, writeData, 3);
   rmmAssertSizeTEqual(runebqCount(&queue), 2);


   rmmAssertSizeTEqual(runebqRead(&queue, readData, dataSize), 2);
   rmmAssertMemoryEqual(readData, writeData + 3, 2);
   rmmAssertSizeTEqual(runebqCount(&queue), 0);
   
   runebqCleanup(&queue);
}

void test_swapBuffers(void)
{
   struct rune_bytequeue queue;
   const size_t dataSize = 5;
   const uint8_t writeData[] = {1, 2, 3, 4, 5};
   uint8_t readData[5] = {0, 0, 0, 0, 0};
   
   runebqInit(&queue, 0, 0);
   
   rmmAssertSizeTEqual(runebqCount(&queue), 0);
   
   // Write 5
   rmmAssertSizeTEqual(runebqWrite(&queue, writeData, dataSize), dataSize);
   rmmAssertSizeTEqual(runebqCount(&queue), dataSize);
   
   // Read 5
   rmmAssertSizeTEqual(runebqRead(&queue, readData, dataSize), dataSize);
   rmmAssertMemoryEqual(readData, writeData, dataSize);
   rmmAssertSizeTEqual(runebqCount(&queue), 0);


   // Write 5
   rmmAssertSizeTEqual(runebqWrite(&queue, writeData, dataSize), dataSize);
   rmmAssertSizeTEqual(runebqCount(&queue), dataSize);
   
   // Read 5
   rmmAssertSizeTEqual(runebqRead(&queue, readData, dataSize), dataSize);
   rmmAssertMemoryEqual(readData, writeData, dataSize);
   rmmAssertSizeTEqual(runebqCount(&queue), 0);


   // Write 5
   rmmAssertSizeTEqual(runebqWrite(&queue, writeData, dataSize), dataSize);
   rmmAssertSizeTEqual(runebqCount(&queue), dataSize);
   
   // Read 5
   rmmAssertSizeTEqual(runebqRead(&queue, readData, dataSize), dataSize);
   rmmAssertMemoryEqual(readData, writeData, dataSize);
   rmmAssertSizeTEqual(runebqCount(&queue), 0);
   
   runebqCleanup(&queue);
}

void test_peek(void)
{
   struct rune_bytequeue queue;
   struct runebq_byteptr end;
   const size_t dataSize = 5;
   const uint8_t writeData[] = {1, 2, 3, 4, 5};
   const uint8_t expectedData[] = {2, 3, 4, 5, 1, 2, 3, 4, 5};
   uint8_t readData[10] = {0};
   
   runebqInit(&queue, 0, 0);
   
   rmmAssertSizeTEqual(runebqCount(&queue), 0);
   
   // Write 5
   rmmAssertSizeTEqual(runebqWrite(&queue, writeData, dataSize), dataSize);
   rmmAssertSizeTEqual(runebqCount(&queue), dataSize);
   
   // Read 1 for the swap
   rmmAssertSizeTEqual(runebqRead(&queue, readData, 1), 1);
   rmmAssertMemoryEqual(readData, writeData, 1);
   rmmAssertSizeTEqual(runebqCount(&queue), 4);


   // Write 5
   rmmAssertSizeTEqual(runebqWrite(&queue, writeData, dataSize), dataSize);
   rmmAssertSizeTEqual(runebqCount(&queue), 9);
   
   // Peek
   rmmAssertSizeTEqual(runebqPeek(&queue, &queue.front, readData, 10, &end), 9);
   rmmAssertMemoryEqual(readData, expectedData, 9);
   rmmAssertSizeTEqual(runebqCount(&queue), 9);


   runebqCleanup(&queue);
}



int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_basicReadAndWrite),
      rmmMakeTest(test_swapBuffers),
      rmmMakeTest(test_peek),
   };
   return rmmRunTestsCmdLine(tests, "List", argc, args);
}


