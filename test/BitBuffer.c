#include <stdint.h>
#include <ryanmock.h>
#include <runeBitBuffer.h>


void test_BitWrite(void)
{
   uint8_t bufferData[4] = {0};
   uint8_t bitsToWrite[] = { 0b00001111, 0b00000001}; 
   struct rune_bit_buffer buffer;

   runebbInitWriter(&buffer, bufferData, 4);

   rmmAssertSizeTEqual(runebbWriteBits(&buffer, bitsToWrite, 9, 1), 9);

   rmmAssertIntU8Equal(bufferData[0], 0x0F);
   rmmAssertIntU8Equal(bufferData[1], 0x01);


   rmmAssertSizeTEqual(runebbWriteBit(&buffer, false), 1);
   rmmAssertSizeTEqual(runebbWriteBit(&buffer, true),  1);
   
   rmmAssertIntU8Equal(bufferData[1], 0x05);
}

void test_BitWriteReadOnly(void)
{
   uint8_t bufferData[4] = {0};
   struct rune_bit_buffer buffer;

   runebbInitReader(&buffer, bufferData, 4);


   rmmAssertSizeTEqual(runebbWriteBit(&buffer, true), 0);
   
   rmmAssertIntU8Equal(bufferData[0], 0x00);
}

void test_BitWriteInRange(void)
{
   uint8_t bufferData[4] = {0};
   uint8_t bitsToWrite[] = { 0b10000000, 0b00000001};
   struct rune_bit_buffer buffer;

   runebbInitWriter(&buffer, bufferData, 1);

   rmmAssertSizeTEqual(runebbWriteBits(&buffer, bitsToWrite, 9, 1), 8);

   rmmAssertIntU8Equal(bufferData[0], 0x80);
   rmmAssertIntU8Equal(bufferData[1], 0x00);
   
   rmmAssertSizeTEqual(runebbWriteBit(&buffer, true), 0);
   
   rmmAssertIntU8Equal(bufferData[1], 0x00);
}


void test_BitRead(void)
{
   uint8_t bufferData[4] = {0x0F, 0x05};
   struct rune_bit_buffer buffer;
   uint8_t expectedBitsToRead[] = { 0b00001111, 0b00000001 };
   uint8_t bitsRead[11];
   bool bit;


   runebbInitReader(&buffer, bufferData, 2);


   rmmAssertSizeTEqual(runebbReadBits(&buffer, bitsRead, 9, 1), 9);
   
   bitsRead[1] &= 0x01;
   
   rmmAssertMemoryEqual(bitsRead, expectedBitsToRead, 2);

   rmmAssertSizeTEqual(runebbReadBit(&buffer, &bit), 1);
   rmmAssertBoolEqual(bit, false);

   rmmAssertSizeTEqual(runebbReadBit(&buffer, &bit), 1);
   rmmAssertBoolEqual(bit, true);

}

void test_BitReadInRange(void)
{
   uint8_t bufferData[1] = {0x55};
   struct rune_bit_buffer buffer;
   uint8_t expectedBitsToRead[] = { 0b01010101 };
   uint8_t bitsRead[9];
   bool bit;


   runebbInitReader(&buffer, bufferData, 1);


   rmmAssertSizeTEqual(runebbReadBits(&buffer, bitsRead, 9, 1), 8);
   
   rmmAssertMemoryEqual(bitsRead, expectedBitsToRead, 1);

   bit = true;
   rmmAssertSizeTEqual(runebbReadBit(&buffer, &bit), 0);
   rmmAssertBoolEqual(bitsRead[0], true);

   bit = false;
   rmmAssertSizeTEqual(runebbReadBit(&buffer, &bit), 0);
   rmmAssertBoolEqual(bit, false);

}

void test_ByteWrite(void)
{
   uint8_t bufferData[4] = {0};
   uint8_t bytesToWrite[4] = { 1, 2, 3, 4 };
   uint8_t expectedBufferData[4] = { 1, 2, 3, 4};
   struct rune_bit_buffer buffer;

   runebbInitWriter(&buffer, bufferData, 4);

   rmmAssertSizeTEqual(runebbWriteBytes(&buffer, bytesToWrite, 4, 1), 4);

   rmmAssertMemoryEqual(bufferData, expectedBufferData, 4);
}

void test_ByteWriteInRange(void)
{
   uint8_t bufferData[4] = {0};
   uint8_t bytesToWrite[4] = { 1, 2, 3, 4 };
   uint8_t expectedBufferData[4] = { 3, 4, 6 };
   struct rune_bit_buffer buffer;

   runebbInitWriter(&buffer, bufferData, 4);

   rmmAssertSizeTEqual(runebbWriteBit(&buffer, true), 1);
   rmmAssertSizeTEqual(runebbWriteBytes(&buffer, bytesToWrite, 4, 1), 3);

   rmmAssertMemoryEqual(bufferData, expectedBufferData, 3);
}

void test_ByteRead(void)
{
   uint8_t bufferData[4] = { 1, 2, 3, 4 };
   uint8_t readBytes[4] = {0};
   uint8_t expectedBufferData[4] = { 1, 2, 3, 4};
   struct rune_bit_buffer buffer;

   runebbInitReader(&buffer, bufferData, 4);

   rmmAssertSizeTEqual(runebbReadBytes(&buffer, readBytes, 4, 1), 4);

   rmmAssertMemoryEqual(readBytes, expectedBufferData, 4);
}

void test_ByteReadInRange(void)
{
   uint8_t bufferData[4] = { 1, 2, 3, 4 };
   uint8_t readBytes[4] = {0};
   uint8_t expectedBufferData[3] = { 0x00, 0x81, 0x01 };
   bool bit;
   struct rune_bit_buffer buffer;

   runebbInitReader(&buffer, bufferData, 4);


   rmmAssertSizeTEqual(runebbReadBit(&buffer, &bit), 1);
   rmmAssertTrue(bit);

   rmmAssertSizeTEqual(runebbReadBytes(&buffer, readBytes, 4, 1), 3);

   rmmAssertMemoryEqual(readBytes, expectedBufferData, 3);
}

void test_DogFood(void)
{
   uint8_t bufferData[32] = {0};
   struct rune_bit_buffer reader, writer;
   bool bit;
   uint8_t byte;

   runebbInitWriter(&writer, bufferData, 32);

   rmmAssertSizeTEqual(runebbWriteBit(&writer, true), 1);
   rmmAssertSizeTEqual(runebbWriteBit(&writer, false), 1);
   rmmAssertSizeTEqual(runebbWriteByte(&writer, 0x55), 1);
   rmmAssertSizeTEqual(runebbWriteByte(&writer, 0xAA), 1);


   runebbInitReader(&reader, bufferData, 32);

   rmmAssertSizeTEqual(runebbReadBit(&reader, &bit), 1);
   rmmAssertTrue(bit);
   rmmAssertSizeTEqual(runebbReadBit(&reader, &bit), 1);
   rmmAssertFalse(bit);
   rmmAssertSizeTEqual(runebbReadByte(&reader, &byte), 1);
   rmmAssertIntU8Equal(byte, 0x55);
   rmmAssertSizeTEqual(runebbReadByte(&reader, &byte), 1);
   rmmAssertIntU8Equal(byte, 0xAA);

}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_BitWrite),
      rmmMakeTest(test_BitWriteReadOnly),
      rmmMakeTest(test_BitWriteInRange),
      rmmMakeTest(test_BitRead),
      rmmMakeTest(test_BitReadInRange),
      rmmMakeTest(test_ByteWrite),
      rmmMakeTest(test_ByteWriteInRange),
      rmmMakeTest(test_ByteRead),
      rmmMakeTest(test_ByteReadInRange),
      rmmMakeTest(test_DogFood),
   };
   return rmmRunTestsCmdLine(tests, "BitBuffer", argc, args);
}


